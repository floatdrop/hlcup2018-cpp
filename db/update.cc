#include "db.h"

using namespace std;

size_t Db::update(const uint32_t id, const Document &info) noexcept
{
    const auto account = accounts[id];

    Account a(account);
    if (info.HasMember("email"))
    {
        string email = info["email"].GetString();
        if (email_tree.find(email) != email_tree.end())
        {
            return 400;
        }

        if (email.find_first_of('@') == string::npos)
        {
            return 400;
        }

        a.email = email;
        a.email_domain = domain_dict[parse_email_domain(email)];
    }

    if (info.HasMember("sex"))
    {
        std::string_view value = info["sex"].GetString();
        if (value != "m" && value != "f")
        {
            return 400;
        }
        a.sex = parse_sex(value);
    }

    if (info.HasMember("status"))
    {
        a.status = parse_status(info["status"].GetString());
        if (a.status == 0)
        {
            return 400;
        }
    }

    if (info.HasMember("city"))
    {
        a.city = cities_dict[info["city"].GetString()];
    }

    if (info.HasMember("country"))
    {
        a.country = countries_dict[info["country"].GetString()];
    }

    if (info.HasMember("birth"))
    {
        if (!info["birth"].IsInt())
        {
            return 400;
        }
        a.birth = info["birth"].GetInt();
    }

    if (info.HasMember("joined"))
    {
        if (!info["joined"].IsInt())
        {
            return 400;
        }
        a.joined = info["joined"].GetInt();
    }

    if (info.HasMember("phone"))
    {
        a.phone = info["phone"].GetString();
        uint32_t pcode;
        if (!absl::SimpleAtoi(a.phone.substr(2, 3), &pcode))
        {
            return 400;
        };

        a.pcode = pcode;
    }

    if (info.HasMember("fname"))
    {
        auto fname = info["fname"].GetString();
        a.fname = fnames_dict[fname];
        sex_names[a.sex].insert(fname);
    }

    if (info.HasMember("sname"))
    {
        a.sname = snames_dict[info["sname"].GetString()];
    }

    if (info.HasMember("likes"))
    {
        auto likes_array = info["likes"].GetArray();

        for (const auto &like : likes_array)
        {
            if (!like.HasMember("id") || !like.HasMember("ts") || !like["id"].IsInt() || !like["ts"].IsInt())
            {
                return 400;
            }

            auto id = like["id"].GetInt();
            if (!account_exist(id))
            {
                return 400;
            }
        }

        for (auto like : a.likes)
        {
            // auto &index = liked_index[like.first];
            // index.erase(remove(index.begin(), index.end(), id), index.end());
            auto &index2 = liked_sex_index[like.first][a.sex];
            index2.erase(remove(index2.begin(), index2.end(), id), index2.end());
        }

        a.likes.clear();
        a.likes.reserve(likes_array.Size());

        for (const auto &like : likes_array)
        {
            auto likee = like["id"].GetInt();
            auto ts = like["ts"].GetInt();
            a.add_like(likee, ts);
            // liked_index[likee].push_back(id);
            liked_sex_index[likee][a.sex].push_back(id);
        }
    }

    if (info.HasMember("premium"))
    {
        if (!info["premium"].IsObject() || !info["premium"].HasMember("start") || !info["premium"].HasMember("finish"))
        {
            return 400;
        }

        a.premium_start = info["premium"]["start"].GetInt();
        a.premium_finish = info["premium"]["finish"].GetInt();

        if (a.premium_start != 0 && a.premium_finish != 0 && a.premium_start < Db::current_time && Db::current_time < a.premium_finish)
        {
            a.premium_now = true;
        }
        else
        {
            a.premium_now = false;
        }
    }

    if (info.HasMember("interests"))
    {
        auto array = info["interests"].GetArray();
        a.interests_bits.reset();
        for (const auto &v : array)
        {
            a.interests_bits.set(interests_dict[v.GetString()]);
        }
    }

    // Update primary index
    if (info.HasMember("email"))
    {
        email_tree.erase(account.email);
        email_tree.insert(a.email);
    }

    accounts[id] = a;

    return 202;
};
