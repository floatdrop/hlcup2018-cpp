#include "db.h"

using namespace std;

struct SuggestCandidate
{
    uint32_t id = 0;
    float similarity = -1;
    //
    // SuggestCandidate(uint32_t id, float similarity) : id(id), similarity(similarity) {}
};

bool operator<(const SuggestCandidate &x, const SuggestCandidate &y)
{
    if (x.similarity < y.similarity)
        return false;
    if (y.similarity < x.similarity)
        return true;

    if (x.id < y.id)
        return false;
    if (y.id < x.id)
        return true;

    return false;
}

inline float similarity_func(uint32_t a, uint32_t b) noexcept
{
    if (a == b)
    {
        return 1.0;
    }
    else if (a > b)
    {
        return 1.0 / (a - b);
    }
    else
    {
        return 1.0 / (b - a);
    }
}

size_t Db::suggest(const uint32_t id, const SimpleWeb::CaseInsensitiveMultimap &query, Writer<StringBuffer> &writer) noexcept
{
    auto limit_iter = query.find("limit");
    if (limit_iter == query.end())
    {
        return 400;
    }
    size_t limit;

    if (!absl::SimpleAtoi(limit_iter->second, &limit) || limit < 1 || limit > 20)
    {
        return 400;
    };

    const auto &target = accounts[id];

    uint16_t city_filter_value = 0;
    uint8_t country_filter_value = 0;

    auto index_size = 0;

    for (const auto &[key, value] : query)
    {
        if (key == "limit" || key == "query_id")
        {
            continue;
        }

        if (key == "country")
        {
            if (value == "")
            {
                return 400;
            }
            auto id = Db::countries_dict[value];
            country_filter_value = id;
        }
        else if (key == "city")
        {
            if (value == "")
            {
                return 400;
            }

            auto id = Db::cities_dict[value];
            city_filter_value = id;
        }
        else
        {
            return 400;
        }
    }

    std::array<SuggestCandidate, 5> candidates;


    for (const auto [i, ts] : target.likes)
    {
        for (const auto cid : liked_sex_index[i][target.sex])
        {
            if (cid == id)
            {
                continue;
            }

            const auto &candidate = accounts[cid];

            if (city_filter_value != 0 && candidate.city != city_filter_value)
            {
                continue;
            }

            if (country_filter_value != 0 && candidate.country != country_filter_value)
            {
                continue;
            }

            if (candidates[0].id != cid && candidates[1].id != cid && candidates[2].id != cid && candidates[3].id != cid && candidates[4].id != cid)
            {
                // float similarity = 0.0;
                // std::vector<uint32_t> intersection;
                // intersection.reserve(2);
                // std::set_intersection(
                //     target.likes_ids.begin(), target.likes_ids.end(),
                //     candidate.likes_ids.begin(), candidate.likes_ids.end(),
                //     std::back_inserter(intersection), std::greater<uint32_t>());

                // for (auto &like_id : intersection)
                // {
                //     similarity += similarity_func(target.likes.find(like_id)->second, candidate.likes.find(like_id)->second);
                // }

                auto similarity = similarity_func(target.likes.find(i)->second, candidate.likes.find(i)->second);
                
                for (auto &[like_id, like_ts] : candidate.likes)
                {
                    if (like_id == i)
                    {
                        continue;
                    }
                    auto iter = target.likes.find(like_id);
                    if (iter != target.likes.end())
                    {
                        similarity += similarity_func(iter->second, candidate.likes.find(like_id)->second);
                        break;
                    }
                }

                candidates[4] = {cid, similarity};
                std::sort(candidates.begin(), candidates.end());
            }
        }
    }

    writer.StartObject();
    writer.Key("accounts");
    writer.StartArray();

    id_set likes;
    likes.reserve(64);

    auto size = 0;
    for (const auto &c : candidates)
    {
        if (size >= limit)
        {
            break;
        }

        likes.clear();
        for (auto &[id, ts] : accounts[c.id].likes)
        {
            likes.push_back(id);
        }
        std::sort(likes.begin(), likes.end(), std::greater<uint32_t>());

        for (const auto id : likes)
        {
            if (size >= limit)
            {
                break;
            }

            if (target.likes.contains(id))
            {
                continue;
            }

            size++;

            const auto &account = accounts[id];

            writer.StartObject();
            writer.Key("id");
            writer.Uint(id);
            writer.Key("email");
            writer.String(StringRef(account.email));
            writer.Key("status");
            writer.String(StringRef(Db::statuses[account.status]));

            if (account.fname != 0)
            {
                writer.Key("fname");
                writer.String(StringRef(fnames_dict[account.fname]));
            }

            if (account.sname != 0)
            {
                writer.Key("sname");
                writer.String(StringRef(snames_dict[account.sname]));
            }

            writer.EndObject();
        }
    }

    writer.EndArray();
    writer.EndObject();

    return 200;
}
