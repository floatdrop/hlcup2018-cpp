#pragma once

#include <vector>
#include <set>
#include <queue>

#include "fmt/format.h"
#include "boost/container/flat_set.hpp"
#include "boost/container/flat_map.hpp"
#include "boost/date_time/posix_time/conversion.hpp"
#include "absl/container/flat_hash_map.h"
#include "absl/container/flat_hash_set.h"
#include "absl/strings/numbers.h"
#include "absl/strings/str_format.h"
#include "absl/strings/string_view.h"
#include "absl/strings/str_split.h"
#include "idmap/idmap.h"
#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/writer.h"
#include "tsl/htrie_set.h"
#include "tsl/htrie_map.h"
#include "server_http.hpp"

using namespace rapidjson;

GenericStringRef<char> StringRef(absl::string_view s);

using id_set = std::vector<uint32_t>;

struct Account
{
  bool sex;
  uint8_t status;
  uint16_t city;
  uint8_t country;

  int32_t birth;
  int32_t joined;
  uint16_t pcode;
  uint16_t fname;
  uint16_t sname;
  uint32_t premium_start;
  uint32_t premium_finish;
  uint8_t email_domain;

  std::string email;

  absl::flat_hash_map<uint32_t, uint32_t> likes;
  // absl::flat_hash_set<uint8_t> interests;

  std::bitset<128> interests_bits;

  void add_like(uint32_t id, uint32_t ts);

  std::string phone;

  bool premium_now;
};

struct Candidate
{
  int32_t age_diff;
  uint32_t id;

  Candidate(int32_t age_diff, uint32_t id) : age_diff(age_diff), id(id) {}
};

inline bool operator<(const Candidate &x, const Candidate &y)
{
  if (x.age_diff < y.age_diff)
    return true;
  if (y.age_diff < x.age_diff)
    return false;

  if (x.id < y.id)
    return true;
  if (y.id < x.id)
    return false;

  return false;
}

struct GroupCounters
{
  uint32_t total_count = 0;

  std::array<uint32_t, 32> birth = {};
  std::array<uint32_t, 24> joined = {};

  std::array<uint32_t, 2> sex = {};
  std::array<std::array<uint32_t, 32>, 2> sex_birth = {};
  std::array<std::array<uint32_t, 24>, 2> sex_joined = {};

  std::array<uint32_t, 2> status = {};
  std::array<std::array<uint32_t, 32>, 4> status_birth = {};
  std::array<std::array<uint32_t, 24>, 4> status_joined = {};

  std::array<uint32_t, 620> city = {};
  std::array<std::array<uint32_t, 32>, 620> city_birth = {};
  std::array<std::array<uint32_t, 24>, 620> city_joined = {};
  std::array<std::array<std::array<uint32_t, 4>, 2>, 620> city_sex_status = {};
  std::array<std::array<std::array<std::array<uint32_t, 32>, 4>, 2>, 620> city_sex_status_birth = {};
  std::array<std::array<std::array<std::array<uint32_t, 24>, 4>, 2>, 620> city_sex_status_joined = {};

  std::array<uint32_t, 80> country = {};
  std::array<std::array<uint32_t, 32>, 80> country_birth = {};
  std::array<std::array<uint32_t, 24>, 80> country_joined = {};
  std::array<std::array<std::array<uint32_t, 4>, 2>, 80> country_sex_status = {};
  std::array<std::array<std::array<std::array<uint32_t, 32>, 4>, 2>, 80> country_sex_status_birth = {};
  std::array<std::array<std::array<std::array<uint32_t, 24>, 4>, 2>, 80> country_sex_status_joined = {};
};

struct InterestsGroupCounters
{
  uint32_t total_count = 0;

  std::array<uint32_t, 32> birth = {};
  std::array<uint32_t, 24> joined = {};
  std::array<uint32_t, 620> city = {};
  std::array<std::array<uint32_t, 32>, 620> city_birth = {};
  std::array<std::array<uint32_t, 24>, 620> city_joined = {};
  std::array<std::array<std::array<uint32_t, 4>, 2>, 620> city_sex_status = {};
  std::array<std::array<std::array<std::array<uint32_t, 32>, 4>, 2>, 620> city_sex_status_birth = {};
  std::array<std::array<std::array<std::array<uint32_t, 24>, 4>, 2>, 620> city_sex_status_joined = {};

  std::array<uint32_t, 80> country = {};
  std::array<std::array<uint32_t, 32>, 80> country_birth = {};
  std::array<std::array<uint32_t, 24>, 80> country_joined = {};
  std::array<std::array<std::array<uint32_t, 4>, 2>, 80> country_sex_status = {};
  std::array<std::array<std::array<std::array<uint32_t, 32>, 4>, 2>, 80> country_sex_status_birth = {};
  std::array<std::array<std::array<std::array<uint32_t, 24>, 4>, 2>, 80> country_sex_status_joined = {};
};

class Db
{
public:
  // Dictionaries
  static IdMap cities_dict;
  static IdMap countries_dict;
  static IdMap fnames_dict;
  static IdMap snames_dict;
  static IdMap interests_dict;
  static IdMap domain_dict;
  static absl::flat_hash_set<std::string> allowed_filter_keys;
  static absl::flat_hash_set<std::string> allowed_group_keys;
  static size_t current_time;
  static constexpr const char *const sexses[] = {"f", "m"};
  static constexpr const char *const statuses[] = {"", "свободны", "всё сложно", "заняты"};

  std::vector<Account> accounts;

  id_set ids_index;

  // Big Indexes
  // absl::flat_hash_map<uint32_t, id_set> liked_index;
  std::vector<std::array<id_set, 2>> liked_sex_index;

  std::vector<id_set> interested_index;
  absl::flat_hash_map<std::pair<uint8_t, uint8_t>, id_set> interested_2_index;
  absl::flat_hash_map<std::tuple<uint8_t, uint8_t, uint8_t, uint8_t, uint8_t>, id_set> interested_5_index;

  // Tiny Indexes
  std::vector<id_set> sex_index;
  std::vector<id_set> status_index;
  std::vector<id_set> status_neq_index;

  absl::flat_hash_map<std::tuple<uint8_t, bool, uint8_t>, id_set> country_sex_status_index;
  absl::flat_hash_map<std::tuple<uint16_t, bool, uint8_t>, id_set> city_sex_status_index;

  id_set premium_now_index;
  id_set premium_null_index;
  id_set premium_nnull_index;

  absl::flat_hash_map<std::tuple<bool, bool, uint8_t, uint8_t>, id_set> premium_sex_status_interest_index;

  std::vector<id_set> city_index;
  id_set city_nnull_index;

  std::vector<id_set> country_index;
  id_set country_nnull_index;

  absl::flat_hash_map<uint16_t, id_set> phone_index;
  id_set phone_nnull_index;

  std::vector<id_set> fname_index;
  id_set fname_nnull_index;

  std::vector<tsl::htrie_set<char>> sex_names;

  std::vector<id_set> sname_index;
  id_set sname_nnull_index;

  // Lt-Gt indexes
  tsl::htrie_set<char> sname_tree;
  tsl::htrie_set<char> email_tree;
  absl::flat_hash_map<std::string, id_set> email_prefix_index;
  std::vector<std::string> email_prefixes;

  absl::flat_hash_map<uint16_t, id_set> birth_index;
  absl::flat_hash_map<uint16_t, id_set> domain_index;

  GroupCounters grouped_counters;
  absl::flat_hash_map<uint8_t, InterestsGroupCounters> interests_grouped_counters;

  Db();

  void index_stats();

  size_t insert(const Document &doc) noexcept;

  size_t update(const uint32_t id, const Document &doc) noexcept;

  size_t likes(const Document &doc) noexcept;

  void reindex();

  size_t filter(const SimpleWeb::CaseInsensitiveMultimap &query, Writer<StringBuffer> &writer) noexcept;
  size_t group(const SimpleWeb::CaseInsensitiveMultimap &query, Writer<StringBuffer> &writer) noexcept;
  absl::flat_hash_map<std::pair<uint16_t, uint16_t>, uint32_t> group_interests(const std::vector<absl::string_view> &keys, const SimpleWeb::CaseInsensitiveMultimap &query) noexcept;
  absl::flat_hash_map<std::pair<uint16_t, uint16_t>, uint32_t> group_interests_filter(const std::vector<absl::string_view> &keys, const SimpleWeb::CaseInsensitiveMultimap &query) noexcept;
  absl::flat_hash_map<std::pair<uint16_t, uint16_t>, uint32_t> group_counters(const std::vector<absl::string_view> &keys, const SimpleWeb::CaseInsensitiveMultimap &query) noexcept;

  size_t recommend(const uint32_t id, const SimpleWeb::CaseInsensitiveMultimap &query, Writer<StringBuffer> &writer) noexcept;
  size_t suggest(const uint32_t id, const SimpleWeb::CaseInsensitiveMultimap &query, Writer<StringBuffer> &writer) noexcept;

  bool account_exist(uint32_t id) noexcept;
};

inline uint8_t parse_status(const std::string_view status)
{
  if (status == "свободны")
  {
    return 1;
  }
  else if (status == "заняты")
  {
    return 3;
  }
  else if (status == "всё сложно")
  {
    return 2;
  }
  else
  {
    return 0;
  }
}

inline bool parse_sex(const std::string_view sex)
{
  if (sex == "f")
  {
    return 0;
  }
  return 1;
}

inline std::string_view parse_email_domain(const std::string_view email)
{
  return email.substr(email.find_first_of('@') + 1, std::string_view::npos);
}

class Predicate
{
public:
  virtual bool operator()(const Account &account) const = 0;
  virtual ~Predicate() = default;
};

struct SexEq : public Predicate
{
  bool sex;
  SexEq(bool sex);
  bool operator()(const Account &account) const;
};

struct StatusEq : public Predicate
{
  uint8_t status;
  StatusEq(uint8_t status);
  bool operator()(const Account &account) const;
};

struct StatusNeq : public Predicate
{
  uint8_t status;
  StatusNeq(uint8_t status);
  bool operator()(const Account &account) const;
};

struct EmailDomainEq : public Predicate
{
  uint16_t domain;
  EmailDomainEq(const uint16_t domain);
  bool operator()(const Account &account) const;
};

struct EmailLt : public Predicate
{
  absl::string_view email;
  EmailLt(const absl::string_view email);
  bool operator()(const Account &account) const;
};

struct EmailGt : public Predicate
{
  absl::string_view email;
  EmailGt(const absl::string_view email);
  bool operator()(const Account &account) const;
};

struct FnameEq : public Predicate
{
  uint16_t fname;
  FnameEq(const absl::string_view name);
  bool operator()(const Account &account) const;
};

struct FnameNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct FnameNotNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct FnameAny : public Predicate
{
  std::vector<uint16_t> set;
  FnameAny(const absl::string_view fnames);
  bool operator()(const Account &account) const;
};

struct SnameEq : public Predicate
{
  uint16_t sname;
  SnameEq(const absl::string_view name);
  bool operator()(const Account &account) const;
};

struct SnameNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct SnameNotNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct SnameStarts : public Predicate
{
  std::vector<absl::string_view> set;
  SnameStarts(const absl::string_view snames);
  bool operator()(const Account &account) const;
};

struct PhoneCode : public Predicate
{
  uint32_t code;
  PhoneCode(const uint32_t c);
  bool operator()(const Account &account) const;
};

struct PhoneNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct PhoneNotNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct BirthLt : public Predicate
{
  int32_t birth;
  BirthLt(const int32_t b);
  bool operator()(const Account &account) const;
};

struct BirthGt : public Predicate
{
  int32_t birth;
  BirthGt(const int32_t b);
  bool operator()(const Account &account) const;
};

struct BirthYear : public Predicate
{
  int32_t year;
  BirthYear(const int32_t y);
  bool operator()(const Account &account) const;
};

struct JoinedYear : public Predicate
{
  int32_t year;
  JoinedYear(const int32_t y);
  bool operator()(const Account &account) const;
};

struct PremiumNow : public Predicate
{
  bool operator()(const Account &account) const;
};

struct PremiumNotNow : public Predicate
{
  bool operator()(const Account &account) const;
};

struct PremiumNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct PremiumNotNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct CountryEq : public Predicate
{
  uint16_t country;
  CountryEq(const uint16_t id);
  bool operator()(const Account &account) const;
};

struct CountryNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct CountryNotNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct CityEq : public Predicate
{
  uint16_t city;
  CityEq(const uint16_t id);
  bool operator()(const Account &account) const;
};

struct CityNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct CityNotNull : public Predicate
{
  bool operator()(const Account &account) const;
};

struct CityAny : public Predicate
{
  std::vector<uint16_t> set;
  CityAny(const absl::string_view cities);
  bool operator()(const Account &account) const;
};

struct InterestsContains : public Predicate
{
  std::bitset<128> set;
  InterestsContains(const absl::string_view interests);
  bool operator()(const Account &account) const;
};

struct InterestsAny : public Predicate
{
  std::bitset<128> set;
  InterestsAny(const absl::string_view interests);
  bool operator()(const Account &account) const;
};

struct LikesContains : public Predicate
{
  std::vector<uint32_t> set;
  LikesContains(const absl::string_view likes);
  bool operator()(const Account &account) const;
};

inline bool match_filters(const std::vector<std::unique_ptr<Predicate>> &filters, const Account &account)
{
  for (const auto &filter : filters)
  {
    if (!(*filter)(account))
    {
      return false;
    }
  }

  return true;
}

inline int32_t year_to_ts(uint16_t year)
{
  using namespace boost::gregorian;
  using namespace boost::posix_time;

  return to_time_t(ptime(date(year, Jan, 1), time_duration(0, 0, 0)));
};

inline uint16_t year_from_ts(int32_t ts)
{
  using namespace boost::gregorian;
  using namespace boost::posix_time;

  ptime pt = from_time_t(ts);
  return pt.date().year();
};
