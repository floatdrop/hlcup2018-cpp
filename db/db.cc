#include "db.h"

using namespace std;

// Dictionaries
IdMap Db::cities_dict;
IdMap Db::countries_dict;
IdMap Db::fnames_dict;
IdMap Db::snames_dict;
IdMap Db::interests_dict;
IdMap Db::domain_dict;
size_t Db::current_time;
absl::flat_hash_set<std::string> Db::allowed_filter_keys = {"limit", "query_id", "sex_eq", "status_eq", "status_neq",
                                                            "email_domain", "email_lt", "email_gt",
                                                            "fname_eq", "fname_null", "fname_any",
                                                            "sname_eq", "sname_null", "sname_starts",
                                                            "phone_code", "phone_null", "birth_year",
                                                            "birth_lt", "birth_gt", "premium_now",
                                                            "premium_null", "country_eq", "country_null",
                                                            "city_eq", "city_null", "city_any",
                                                            "interests_any", "interests_contains", "likes_contains"};

absl::flat_hash_set<std::string> Db::allowed_group_keys = {"limit", "query_id", "keys", "order", "sex", "status", "birth", "joined", "city", "country", "likes", "interests"};

GenericStringRef<char> StringRef(absl::string_view s)
{
    return rapidjson::GenericStringRef<char>(s.data(), s.size());
}

void Account::add_like(uint32_t liked, uint32_t ts)
{
    if (likes.contains(liked))
    {
        ts = (ts + likes[liked]) / 2;
    }
    likes.insert_or_assign(liked, ts);
}

Db::Db()
{
    size_t MAX = 1400000;

    accounts.resize(MAX);

    ids_index.reserve(MAX);
    // liked_index.resize(MAX);
    liked_sex_index.resize(MAX);
    interested_index.resize(128);
    interests_grouped_counters.reserve(128);

    sex_index.resize(2);
    sex_index[0].reserve(MAX);
    sex_index[1].reserve(MAX);

    sex_names.resize(2);

    status_index.resize(4);
    status_index[1].reserve(MAX);
    status_index[2].reserve(MAX);
    status_index[3].reserve(MAX);

    status_neq_index.resize(4);
    status_neq_index[1].reserve(MAX);
    status_neq_index[2].reserve(MAX);
    status_neq_index[3].reserve(MAX);

    premium_now_index.reserve(MAX);
    premium_null_index.reserve(MAX);
    premium_nnull_index.reserve(MAX);

    city_index.resize(620);
    country_index.resize(80);
    fname_index.resize(2048);
    sname_index.resize(2048);

    city_nnull_index.reserve(MAX);
    country_nnull_index.reserve(MAX);
    phone_nnull_index.reserve(MAX);
    fname_nnull_index.reserve(MAX);
    sname_nnull_index.reserve(MAX);
}

void Db::index_stats()
{
    // printf("sex_index[f]: %d\n", sex_index[0].size());
    // printf("sex_index[m]: %d\n", sex_index[1].size());

    // printf("status_index[1]: %d\n", status_index[1].size());
    // printf("status_index[2]: %d\n", status_index[2].size());
    // printf("status_index[3]: %d\n", status_index[3].size());

    // printf("country_sex_status_index[Румция, f, 2]: %d\n", country_sex_status_index[{countries_dict["Румция"], false, 2}].size());

    // printf("status_neq_index[1]: %d\n", status_neq_index[1].size());
    // printf("status_neq_index[2]: %d\n", status_neq_index[2].size());
    // printf("status_neq_index[3]: %d\n", status_neq_index[3].size());

    // printf("premium_now_index: %d\n", premium_now_index.size());
    // printf("premium_null_index: %d\n", premium_null_index.size());
    // printf("premium_nnull_index: %d\n", premium_nnull_index.size());

    // printf("city_nnull_index: %d\n", city_nnull_index.size());
    // printf("country_nnull_index: %d\n", country_nnull_index.size());
    // printf("phone_nnull_index: %d\n", phone_nnull_index.size());
    // printf("fname_nnull_index: %d\n", fname_nnull_index.size());
    // printf("sname_nnull_index: %d\n", sname_nnull_index.size());

    // printf("cities: %d\n", cities_dict.size());
    // printf("countries: %d\n", countries_dict.size());
    // printf("interests: %d\n", interests_dict.size());
    // printf("birth: %d\n", birth_index.size());

    // for (auto &[year, idx]: birth_index) {
    //     printf("%d %d\n", year, idx.size());
    // }

    // for (auto &[year, idx]: joined_index) {
    //     printf("%d %d\n", year, idx.size());
    // }

    // printf("domain_dict: %d\n", domain_dict.size());
}

bool Db::account_exist(uint32_t id) noexcept
{
    return id < accounts.size() && accounts[id].email != "";
}

SexEq::SexEq(bool sex) : sex(sex){};

bool SexEq::operator()(const Account &account) const
{
    return account.sex == sex;
};

StatusEq::StatusEq(uint8_t status) : status(status){};

bool StatusEq::operator()(const Account &account) const
{
    return account.status == status;
};

StatusNeq::StatusNeq(uint8_t status) : status(status){};

bool StatusNeq::operator()(const Account &account) const
{
    return account.status != status;
};

EmailDomainEq::EmailDomainEq(const uint16_t domain) : domain(domain){};

bool EmailDomainEq::operator()(const Account &account) const
{
    return account.email_domain == domain;
};

EmailLt::EmailLt(const absl::string_view email) : email(email){};

bool EmailLt::operator()(const Account &account) const
{
    return account.email < email;
};

EmailGt::EmailGt(const absl::string_view email) : email(email){};

bool EmailGt::operator()(const Account &account) const
{
    return account.email > email;
};

FnameEq::FnameEq(const absl::string_view name)
{
    fname = Db::fnames_dict[name];
};

bool FnameEq::operator()(const Account &account) const
{
    return account.fname == fname;
};

bool FnameNull::operator()(const Account &account) const
{
    return account.fname == 0;
};

bool FnameNotNull::operator()(const Account &account) const
{
    return account.fname != 0;
};

FnameAny::FnameAny(const absl::string_view fnames)
{
    for (const auto &name : absl::StrSplit(fnames, ','))
    {
        set.push_back(Db::fnames_dict[name]);
    }
};

bool FnameAny::operator()(const Account &account) const
{
    return find(set.begin(), set.end(), account.fname) != set.end();
};

SnameEq::SnameEq(const absl::string_view name)
{
    sname = Db::snames_dict[name];
};

bool SnameEq::operator()(const Account &account) const
{
    return account.sname == sname;
};

bool SnameNull::operator()(const Account &account) const
{
    return account.sname == 0;
};

bool SnameNotNull::operator()(const Account &account) const
{
    return account.sname != 0;
};

SnameStarts::SnameStarts(const absl::string_view snames)
{
    set = absl::StrSplit(snames, ',');
};

bool SnameStarts::operator()(const Account &account) const
{
    for (const auto &name : set)
    {
        if (Db::snames_dict[account.sname].substr(0, name.size()) == name)
        {
            return true;
        }
    }

    return false;
};

PhoneCode::PhoneCode(const uint32_t c) : code(c){};

bool PhoneCode::operator()(const Account &account) const
{
    return account.pcode == code;
};

bool PhoneNull::operator()(const Account &account) const
{
    return account.pcode == 0;
};

bool PhoneNotNull::operator()(const Account &account) const
{
    return account.pcode != 0;
};

BirthLt::BirthLt(const int32_t b) : birth(b){};

bool BirthLt::operator()(const Account &account) const
{
    return account.birth < birth;
};

BirthGt::BirthGt(const int32_t b) : birth(b){};

bool BirthGt::operator()(const Account &account) const
{
    return account.birth > birth;
};

BirthYear::BirthYear(const int32_t y) : year(y){};

bool BirthYear::operator()(const Account &account) const
{
    return year_from_ts(account.birth) == year;
};

JoinedYear::JoinedYear(const int32_t y) : year(y){};

bool JoinedYear::operator()(const Account &account) const
{
    return year_from_ts(account.joined) == year;
};

bool PremiumNow::operator()(const Account &account) const
{
    return account.premium_now;
};

bool PremiumNotNow::operator()(const Account &account) const
{
    return !account.premium_now;
};

bool PremiumNull::operator()(const Account &account) const
{
    return account.premium_start == 0;
};

bool PremiumNotNull::operator()(const Account &account) const
{
    return account.premium_start != 0;
};

CountryEq::CountryEq(const uint16_t id)
{
    country = id;
};

bool CountryEq::operator()(const Account &account) const
{
    return account.country == country;
};

bool CountryNull::operator()(const Account &account) const
{
    return account.country == 0;
};

bool CountryNotNull::operator()(const Account &account) const
{
    return account.country != 0;
};

CityEq::CityEq(const uint16_t id)
{
    city = id;
};

bool CityEq::operator()(const Account &account) const
{
    return account.city == city;
};

bool CityNull::operator()(const Account &account) const
{
    return account.city == 0;
};

bool CityNotNull::operator()(const Account &account) const
{
    return account.city != 0;
};

CityAny::CityAny(const absl::string_view cities)
{
    for (const auto &city : absl::StrSplit(cities, ','))
    {
        set.push_back(Db::cities_dict[city]);
    }
};

bool CityAny::operator()(const Account &account) const
{
    return find(set.begin(), set.end(), account.city) != set.end();
};

InterestsContains::InterestsContains(const absl::string_view interests)
{
    for (const auto &interest : absl::StrSplit(interests, ','))
    {
        set.set(Db::interests_dict[interest]);
    }
};

bool InterestsContains::operator()(const Account &account) const
{
    return (account.interests_bits & set) == set;
};

InterestsAny::InterestsAny(const absl::string_view interests)
{
    for (const auto &interest : absl::StrSplit(interests, ','))
    {
        set.set(Db::interests_dict[interest]);
    }
};

bool InterestsAny::operator()(const Account &account) const
{
    return (account.interests_bits & set).any();
};

LikesContains::LikesContains(const absl::string_view likes)
{
    for (const auto &like : absl::StrSplit(likes, ','))
    {
        uint32_t l;
        if (absl::SimpleAtoi(like, &l))
        {
            set.push_back(l);
        }
    }
};

bool LikesContains::operator()(const Account &account) const
{
    for (const auto &like : set)
    {
        if (account.likes.find(like) == account.likes.end())
        {
            return false;
        }
    }
    return true;
};
