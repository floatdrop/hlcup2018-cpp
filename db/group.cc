#include "db.h"

using namespace std;
using namespace rapidjson;

bool has_any_filters(const SimpleWeb::CaseInsensitiveMultimap &query) noexcept
{
    for (const auto &[key, value] : query)
    {
        if (key == "limit" || key == "query_id" || key == "keys" || key == "order")
        {
            continue;
        }

        if (key == "sex" || key == "status" || key == "birth" || key == "joined" || key == "country" || key == "city" || key == "interests")
        {
            return true;
        }
    }
    return false;
}

tuple<int32_t, int32_t, int32_t, int32_t, int32_t, int32_t> parse_group_filters(const SimpleWeb::CaseInsensitiveMultimap &query) noexcept
{
    int32_t sex_filter_value = -1;
    int32_t status_filter_value = -1;
    int32_t city_filter_value = -1;
    int32_t country_filter_value = -1;
    int32_t birth_filter_value = -1;
    int32_t joined_filter_value = -1;

    for (const auto &[key, value] : query)
    {
        if (key == "limit" || key == "query_id" || key == "keys" || key == "order")
        {
            continue;
        }

        if (key == "sex")
        {
            sex_filter_value = parse_sex(value);
        }
        else if (key == "status")
        {
            status_filter_value = parse_status(value);
        }
        else if (key == "birth")
        {
            if (!absl::SimpleAtoi(value, &birth_filter_value))
            {
                printf("unexpected birth: %s\n", std::string(value).c_str());
            }

            birth_filter_value -= 1970;
        }
        else if (key == "joined")
        {
            if (!absl::SimpleAtoi(value, &joined_filter_value))
            {
                printf("unexpected joined: %s\n", std::string(value).c_str());
            }

            joined_filter_value -= 2000;
        }
        else if (key == "country")
        {
            country_filter_value = Db::countries_dict[value];
        }
        else if (key == "city")
        {
            city_filter_value = Db::cities_dict[value];
        }
    }

    return {
        sex_filter_value,
        status_filter_value,
        city_filter_value,
        country_filter_value,
        birth_filter_value,
        joined_filter_value};
};

uint32_t get_value(const absl::string_view key, const Account &account) noexcept
{
    if (key == "sex")
    {
        return account.sex;
    }
    else if (key == "status")
    {
        return account.status;
    }
    else if (key == "city")
    {
        return account.city;
    }
    else if (key == "country")
    {
        return account.country;
    }

    return 0;
}

absl::string_view get_string(const absl::string_view key, uint32_t value) noexcept
{
    if (key == "sex")
    {
        return Db::sexses[value];
    }
    else if (key == "status")
    {
        return Db::statuses[value];
    }
    else if (key == "city")
    {
        return Db::cities_dict[value];
    }
    else if (key == "country")
    {
        return Db::countries_dict[value];
    }
    else if (key == "interests")
    {
        return Db::interests_dict[value];
    }

    return "";
}

uint32_t get_int(const absl::string_view key, const string_view value) noexcept
{
    if (key == "sex")
    {
        return parse_sex(value);
    }
    else if (key == "status")
    {
        return parse_status(value);
    }
    else if (key == "city")
    {
        return Db::cities_dict[value];
    }
    else if (key == "country")
    {
        return Db::countries_dict[value];
    }
    else if (key == "interests")
    {
        return Db::interests_dict[value];
    }

    return 0;
}

absl::flat_hash_map<pair<uint16_t, uint16_t>, uint32_t> Db::group_interests_filter(const vector<absl::string_view> &keys, const SimpleWeb::CaseInsensitiveMultimap &query) noexcept
{
    const auto [sex_filter_value, status_filter_value, city_filter_value, country_filter_value, birth_filter_value, joined_filter_value] = parse_group_filters(query);

    absl::flat_hash_map<pair<uint16_t, uint16_t>, uint32_t> groups;

    auto interest = interests_dict[query.find("interests")->second];
    auto &icounters = interests_grouped_counters[interest];

    if (keys.size() == 1 && keys[0] == "city")
    {
        for (size_t city = 0; city < city_index.size(); ++city)
        {
            auto &counter = groups[{city, 0}];
            if (birth_filter_value != -1)
            {
                counter += icounters.city_birth[city][birth_filter_value];
            }
            else if (joined_filter_value != -1)
            {
                counter += icounters.city_joined[city][joined_filter_value];
            }
            else
            {
                counter += icounters.city[city];
            }
        }
    }
    else if (keys.size() == 1 && keys[0] == "country")
    {
        for (size_t country = 0; country < country_index.size(); ++country)
        {
            auto &counter = groups[{country, 0}];
            if (birth_filter_value != -1)
            {
                counter += icounters.country_birth[country][birth_filter_value];
            }
            else if (joined_filter_value != -1)
            {
                counter += icounters.country_joined[country][joined_filter_value];
            }
            else
            {
                counter += icounters.country[country];
            }
        }
    }
    else if (keys.size() == 2 && keys[0] == "city" && keys[1] == "sex")
    {
        for (size_t city = 0; city < city_index.size(); ++city)
        {
            for (auto sex : {0, 1})
            {
                auto &counter = groups[{city, sex}];
                for (auto status : {1, 2, 3})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += icounters.city_sex_status_birth[city][sex][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += icounters.city_sex_status_joined[city][sex][status][joined_filter_value];
                    }
                    else
                    {
                        counter += icounters.city_sex_status[city][sex][status];
                    }
                }
            }
        }
    }
    else if (keys.size() == 2 && keys[0] == "city" && keys[1] == "status")
    {
        for (size_t city = 0; city < city_index.size(); ++city)
        {
            for (auto status : {1, 2, 3})
            {
                auto &counter = groups[{city, status}];
                for (auto sex : {0, 1})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += icounters.city_sex_status_birth[city][sex][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += icounters.city_sex_status_joined[city][sex][status][joined_filter_value];
                    }
                    else
                    {
                        counter += icounters.city_sex_status[city][sex][status];
                    }
                }
            }
        }
    }
    else if (keys.size() == 2 && keys[0] == "country" && keys[1] == "sex")
    {
        for (size_t country = 0; country < country_index.size(); ++country)
        {
            for (auto sex : {0, 1})
            {
                auto &counter = groups[{country, sex}];
                for (auto status : {1, 2, 3})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += icounters.country_sex_status_birth[country][sex][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += icounters.country_sex_status_joined[country][sex][status][joined_filter_value];
                    }
                    else
                    {
                        counter += icounters.country_sex_status[country][sex][status];
                    }
                }
            }
        }
    }
    else if (keys.size() == 2 && keys[0] == "country" && keys[1] == "status")
    {
        for (size_t country = 0; country < country_index.size(); ++country)
        {
            for (auto status : {1, 2, 3})
            {
                auto &counter = groups[{country, status}];
                for (auto sex : {0, 1})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += icounters.country_sex_status_birth[country][sex][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += icounters.country_sex_status_joined[country][sex][status][joined_filter_value];
                    }
                    else
                    {
                        counter += icounters.country_sex_status[country][sex][status];
                    }
                }
            }
        }
    }

    return groups;
};

absl::flat_hash_map<pair<uint16_t, uint16_t>, uint32_t> Db::group_interests(const vector<absl::string_view> &keys, const SimpleWeb::CaseInsensitiveMultimap &query) noexcept
{
    const auto [sex_filter_value, status_filter_value, city_filter_value, country_filter_value, birth_filter_value, joined_filter_value] = parse_group_filters(query);

    absl::flat_hash_map<pair<uint16_t, uint16_t>, uint32_t> groups;

    for (auto &[i, icounters] : interests_grouped_counters)
    {
        auto &counter = groups[{i, 0}];
        if (city_filter_value != -1)
        {
            if (birth_filter_value != -1)
            {
                counter += icounters.city_birth[city_filter_value][birth_filter_value];
            }
            else if (joined_filter_value != -1)
            {
                counter += icounters.city_joined[city_filter_value][joined_filter_value];
            }
            else
            {
                counter += icounters.city[city_filter_value];
            }
        }
        else if (country_filter_value != -1)
        {
            if (birth_filter_value != -1)
            {
                counter += icounters.country_birth[country_filter_value][birth_filter_value];
            }
            else if (joined_filter_value != -1)
            {
                counter += icounters.country_joined[country_filter_value][joined_filter_value];
            }
            else
            {
                counter += icounters.country[country_filter_value];
            }
        }
        else if (birth_filter_value != -1)
        {
            counter += icounters.birth[birth_filter_value];
        }
        else if (joined_filter_value != -1)
        {
            counter += icounters.joined[joined_filter_value];
        }
        else
        {
            counter += icounters.total_count;
        }
    }

    return groups;
};

absl::flat_hash_map<pair<uint16_t, uint16_t>, uint32_t> Db::group_counters(const vector<absl::string_view> &keys, const SimpleWeb::CaseInsensitiveMultimap &query) noexcept
{
    const auto [sex_filter_value, status_filter_value, city_filter_value, country_filter_value, birth_filter_value, joined_filter_value] = parse_group_filters(query);

    absl::flat_hash_map<pair<uint16_t, uint16_t>, uint32_t> groups;

    if (keys.size() == 1 && keys[0] == "sex")
    {
        if (city_filter_value != -1)
        {
            for (auto sex : {0, 1})
            {
                auto &counter = groups[{sex, 0}];
                for (auto status : {1, 2, 3})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_birth[city_filter_value][sex][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_joined[city_filter_value][sex][status][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.city_sex_status[city_filter_value][sex][status];
                    }
                }
            }
        }
        else if (country_filter_value != -1)
        {
            for (auto sex : {0, 1})
            {
                auto &counter = groups[{sex, 0}];
                for (auto status : {1, 2, 3})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_birth[country_filter_value][sex][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_joined[country_filter_value][sex][status][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.country_sex_status[country_filter_value][sex][status];
                    }
                }
            }
        }
        else
        {
            for (auto sex : {0, 1})
            {
                auto &counter = groups[{sex, 0}];
                if (birth_filter_value != -1)
                {
                    counter += grouped_counters.sex_birth[sex][birth_filter_value];
                }
                else if (joined_filter_value != -1)
                {
                    counter += grouped_counters.sex_joined[sex][joined_filter_value];
                }
                else
                {
                    counter += grouped_counters.sex[sex];
                }
            }
        }
    }
    else if (keys.size() == 1 && keys[0] == "status")
    {
        if (city_filter_value != -1)
        {
            for (auto status : {1, 2, 3})
            {
                auto &counter = groups[{status, 0}];
                for (auto sex : {0, 1})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_birth[city_filter_value][sex][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_joined[city_filter_value][sex][status][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.city_sex_status[city_filter_value][sex][status];
                    }
                }
            }
        }
        else if (country_filter_value != -1)
        {
            for (auto status : {1, 2, 3})
            {
                auto &counter = groups[{status, 0}];
                for (auto sex : {0, 1})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_birth[country_filter_value][sex][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_joined[country_filter_value][sex][status][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.country_sex_status[country_filter_value][sex][status];
                    }
                }
            }
        }
        else
        {
            for (auto status : {1, 2, 3})
            {
                auto &counter = groups[{status, 0}];
                if (birth_filter_value != -1)
                {
                    counter += grouped_counters.status_birth[status][birth_filter_value];
                }
                else if (joined_filter_value != -1)
                {
                    counter += grouped_counters.status_joined[status][joined_filter_value];
                }
                else
                {
                    counter += grouped_counters.status[status];
                }
            }
        }
    }
    else if (keys.size() == 1 && keys[0] == "city")
    {
        for (size_t city = 0; city < city_index.size(); ++city)
        {
            auto &counter = groups[{city, 0}];
            if (sex_filter_value != -1)
            {
                for (auto status : {1, 2, 3})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_birth[city][sex_filter_value][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_joined[city][sex_filter_value][status][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.city_sex_status[city][sex_filter_value][status];
                    }
                }
            }
            else if (status_filter_value != -1)
            {
                for (auto sex : {0, 1})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_birth[city][sex][status_filter_value][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_joined[city][sex][status_filter_value][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.city_sex_status[city][sex][status_filter_value];
                    }
                }
            }
            else
            {
                if (birth_filter_value != -1)
                {
                    counter += grouped_counters.city_birth[city][birth_filter_value];
                }
                else if (joined_filter_value != -1)
                {
                    counter += grouped_counters.city_joined[city][joined_filter_value];
                }
                else
                {
                    counter += grouped_counters.city[city];
                }
            }
        }
    }
    else if (keys.size() == 1 && keys[0] == "country")
    {
        for (size_t country = 0; country < country_index.size(); ++country)
        {
            auto &counter = groups[{country, 0}];
            if (sex_filter_value != -1)
            {
                for (auto status : {1, 2, 3})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_birth[country][sex_filter_value][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_joined[country][sex_filter_value][status][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.country_sex_status[country][sex_filter_value][status];
                    }
                }
            }
            else if (status_filter_value != -1)
            {
                for (auto sex : {0, 1})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_birth[country][sex][status_filter_value][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_joined[country][sex][status_filter_value][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.country_sex_status[country][sex][status_filter_value];
                    }
                }
            }
            else
            {
                if (birth_filter_value != -1)
                {
                    counter += grouped_counters.country_birth[country][birth_filter_value];
                }
                else if (joined_filter_value != -1)
                {
                    counter += grouped_counters.country_joined[country][joined_filter_value];
                }
                else
                {
                    counter += grouped_counters.country[country];
                }
            }
        }
    }
    else if (keys.size() == 2 && keys[0] == "city" && keys[1] == "sex")
    {
        if (sex_filter_value != -1)
        {
            for (size_t city = 0; city < city_index.size(); ++city)
            {
                auto &counter = groups[{city, sex_filter_value}];
                for (auto status : {1, 2, 3})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_birth[city][sex_filter_value][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_joined[city][sex_filter_value][status][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.city_sex_status[city][sex_filter_value][status];
                    }
                }
            }
        }
        else if (status_filter_value != -1)
        {
            for (size_t city = 0; city < city_index.size(); ++city)
            {
                for (auto sex : {0, 1})
                {
                    auto &counter = groups[{city, sex}];
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_birth[city][sex][status_filter_value][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_joined[city][sex][status_filter_value][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.city_sex_status[city][sex][status_filter_value];
                    }
                }
            }
        }
        else
        {
            for (size_t city = 0; city < city_index.size(); ++city)
            {
                for (auto sex : {0, 1})
                {
                    auto &counter = groups[{city, sex}];
                    for (auto status : {1, 2, 3})
                    {
                        if (birth_filter_value != -1)
                        {
                            counter += grouped_counters.city_sex_status_birth[city][sex][status][birth_filter_value];
                        }
                        else if (joined_filter_value != -1)
                        {
                            counter += grouped_counters.city_sex_status_joined[city][sex][status][joined_filter_value];
                        }
                        else
                        {
                            counter += grouped_counters.city_sex_status[city][sex][status];
                        }
                    }
                }
            }
        }
    }
    else if (keys.size() == 2 && keys[0] == "city" && keys[1] == "status")
    {
        if (sex_filter_value != -1)
        {
            for (size_t city = 0; city < city_index.size(); ++city)
            {
                for (auto status : {1, 2, 3})
                {
                    auto &counter = groups[{city, status}];
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_birth[city][sex_filter_value][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_joined[city][sex_filter_value][status][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.city_sex_status[city][sex_filter_value][status];
                    }
                }
            }
        }
        else if (status_filter_value != -1)
        {
            for (size_t city = 0; city < city_index.size(); ++city)
            {
                auto &counter = groups[{city, status_filter_value}];
                for (auto sex : {0, 1})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_birth[city][sex][status_filter_value][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.city_sex_status_joined[city][sex][status_filter_value][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.city_sex_status[city][sex][status_filter_value];
                    }
                }
            }
        }
        else
        {
            for (size_t city = 0; city < city_index.size(); ++city)
            {
                for (auto status : {1, 2, 3})
                {
                    auto &counter = groups[{city, status}];
                    for (auto sex : {0, 1})
                    {
                        if (birth_filter_value != -1)
                        {
                            counter += grouped_counters.city_sex_status_birth[city][sex][status][birth_filter_value];
                        }
                        else if (joined_filter_value != -1)
                        {
                            counter += grouped_counters.city_sex_status_joined[city][sex][status][joined_filter_value];
                        }
                        else
                        {
                            counter += grouped_counters.city_sex_status[city][sex][status];
                        }
                    }
                }
            }
        }
    }
    else if (keys.size() == 2 && keys[0] == "country" && keys[1] == "sex")
    {
        if (sex_filter_value != -1)
        {
            for (size_t country = 0; country < country_index.size(); ++country)
            {
                auto &counter = groups[{country, sex_filter_value}];
                for (auto status : {1, 2, 3})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_birth[country][sex_filter_value][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_joined[country][sex_filter_value][status][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.country_sex_status[country][sex_filter_value][status];
                    }
                }
            }
        }
        else if (status_filter_value != -1)
        {
            for (size_t country = 0; country < country_index.size(); ++country)
            {
                for (auto sex : {0, 1})
                {
                    auto &counter = groups[{country, sex}];
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_birth[country][sex][status_filter_value][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_joined[country][sex][status_filter_value][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.country_sex_status[country][sex][status_filter_value];
                    }
                }
            }
        }
        else
        {
            for (size_t country = 0; country < country_index.size(); ++country)
            {
                for (auto sex : {0, 1})
                {
                    auto &counter = groups[{country, sex}];
                    for (auto status : {1, 2, 3})
                    {
                        if (birth_filter_value != -1)
                        {
                            counter += grouped_counters.country_sex_status_birth[country][sex][status][birth_filter_value];
                        }
                        else if (joined_filter_value != -1)
                        {
                            counter += grouped_counters.country_sex_status_joined[country][sex][status][joined_filter_value];
                        }
                        else
                        {
                            counter += grouped_counters.country_sex_status[country][sex][status];
                        }
                    }
                }
            }
        }
    }
    else if (keys.size() == 2 && keys[0] == "country" && keys[1] == "status")
    {
        if (sex_filter_value != -1)
        {
            for (size_t country = 0; country < country_index.size(); ++country)
            {
                for (auto status : {1, 2, 3})
                {
                    auto &counter = groups[{country, status}];
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_birth[country][sex_filter_value][status][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_joined[country][sex_filter_value][status][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.country_sex_status[country][sex_filter_value][status];
                    }
                }
            }
        }
        else if (status_filter_value != -1)
        {
            for (size_t country = 0; country < country_index.size(); ++country)
            {
                auto &counter = groups[{country, status_filter_value}];
                for (auto sex : {0, 1})
                {
                    if (birth_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_birth[country][sex][status_filter_value][birth_filter_value];
                    }
                    else if (joined_filter_value != -1)
                    {
                        counter += grouped_counters.country_sex_status_joined[country][sex][status_filter_value][joined_filter_value];
                    }
                    else
                    {
                        counter += grouped_counters.country_sex_status[country][sex][status_filter_value];
                    }
                }
            }
        }
        else
        {
            for (size_t country = 0; country < country_index.size(); ++country)
            {
                for (auto status : {1, 2, 3})
                {
                    auto &counter = groups[{country, status}];
                    for (auto sex : {0, 1})
                    {
                        if (birth_filter_value != -1)
                        {
                            counter += grouped_counters.country_sex_status_birth[country][sex][status][birth_filter_value];
                        }
                        else if (joined_filter_value != -1)
                        {
                            counter += grouped_counters.country_sex_status_joined[country][sex][status][joined_filter_value];
                        }
                        else
                        {
                            counter += grouped_counters.country_sex_status[country][sex][status];
                        }
                    }
                }
            }
        }
    }

    return groups;
};

size_t Db::group(const SimpleWeb::CaseInsensitiveMultimap &query, Writer<StringBuffer> &writer) noexcept
{
    auto limit_iter = query.find("limit");
    if (limit_iter == query.end())
    {
        return 400;
    }
    int32_t limit;

    if (!absl::SimpleAtoi(limit_iter->second, &limit) || limit < 1 || limit > 50)
    {
        return 400;
    };

    auto order_iter = query.find("order");
    if (order_iter == query.end())
    {
        return 400;
    }

    bool asc_order;
    if (order_iter->second == "1")
    {
        asc_order = true;
    }
    else if (order_iter->second == "-1")
    {
        asc_order = false;
    }
    else
    {
        return 400;
    }

    auto keys_iter = query.find("keys");
    if (keys_iter == query.end())
    {
        return 400;
    }

    auto keys_string = keys_iter->second;
    std::vector<absl::string_view> keys = absl::StrSplit(keys_string, ',');

    for (const auto &key : keys)
    {
        if (key != "sex" && key != "status" && key != "city" && key != "country" && key != "interests")
        {
            return 400;
        }
    }

    absl::flat_hash_map<pair<uint16_t, uint16_t>, uint32_t> groups;

    if (query.find("likes") != query.end())
    {
        vector<std::unique_ptr<Predicate>> filters;
        std::vector<id_set *>indexes;

        absl::flat_hash_map<std::string_view, std::string_view> q;

        for (const auto &[key, value] : query)
        {
            if (!Db::allowed_group_keys.contains(key))
            {
                return 400;
            }
            q[key] = value;
        }

        if (q.contains("sex"))
        {
            filters.push_back(std::make_unique<SexEq>(parse_sex(q["sex"])));
        }
        
        if (q.contains("status"))
        {
            filters.push_back(std::make_unique<StatusEq>(parse_status(q["status"])));
        }
        
        if (q.contains("birth"))
        {
            uint32_t year;
            if (!absl::SimpleAtoi(q["birth"], &year))
            {
                return 400;
            }
            filters.push_back(std::make_unique<BirthYear>(year));
        }
        
        if (q.contains("joined"))
        {
            uint32_t year;
            if (!absl::SimpleAtoi(q["joined"], &year))
            {
                return 400;
            }
            filters.push_back(std::make_unique<JoinedYear>(year));
        }
        
        if (q.contains("country"))
        {
            filters.push_back(std::make_unique<CountryEq>(Db::countries_dict[q["country"]]));
        }
        
        if (q.contains("city"))
        {
            filters.push_back(std::make_unique<CityEq>(Db::cities_dict[q["city"]]));
        }
        
        if (q.contains("interests"))
        {
            filters.push_back(std::make_unique<InterestsAny>(q["interests"]));
        }
        
        if (q.contains("likes"))
        {
            uint32_t id;
            if (!absl::SimpleAtoi(q["likes"], &id))
            {
                return 400;
            }
            if (q.contains("sex")) {
                indexes = {&liked_sex_index[id][parse_sex(q["sex"])]};
            } else {
                indexes = {&liked_sex_index[id][false], &liked_sex_index[id][true]};
            }
        }
        

        for (const auto index : indexes) {
            for (const auto id : *index)
            {
                const auto &account = accounts[id];

                if (!match_filters(filters, account))
                {
                    continue;
                }

                if (keys[0] == "interests")
                {
                    for (size_t i = 0; i < 128; i++)
                    {
                        if (account.interests_bits.test(i))
                        {
                            groups[{i, 0}] += 1;
                        }
                    }
                }
                else
                {
                    uint32_t primary_group = get_value(keys[0], account);
                    uint32_t secondary_group = 0;

                    if (keys.size() > 1)
                    {
                        secondary_group = get_value(keys[1], account);
                    }

                    groups[{primary_group, secondary_group}] += 1;
                }
            }
        }
    }
    else if (keys[0] == "interests")
    {
        groups = group_interests(keys, query);
    }
    else if (query.find("interests") != query.end())
    {
        groups = group_interests_filter(keys, query);
    }
    else
    {
        groups = group_counters(keys, query);
    }

    using Key = tuple<uint32_t, absl::string_view, absl::string_view>;

    std::vector<Key> vec;
    vec.reserve(groups.size());

    for (const auto &[group, count] : groups)
    {
        if (count == 0)
        {
            continue;
        }

        vec.emplace_back(
            count,
            get_string(keys[0], group.first),
            keys.size() > 1 ? get_string(keys[1], group.second) : "");
    }

    if (limit > vec.size())
    {
        limit = vec.size();
    }

    if (asc_order)
    {
        partial_sort(vec.begin(), vec.begin() + limit, vec.end(), std::less<Key>());
    }
    else
    {
        partial_sort(vec.begin(), vec.begin() + limit, vec.end(), std::greater<Key>());
    }

    writer.StartObject();
    writer.Key("groups");
    writer.StartArray();

    auto size = 0;
    for (const auto &[count, key1, key2] : vec)
    {
        if (size >= limit)
        {
            break;
        }

        size++;

        writer.StartObject();
        writer.Key("count");
        writer.Uint(count);
        if (!key1.empty())
        {
            writer.Key(StringRef(keys[0]), keys[0].size());
            writer.String(StringRef(key1));
        }

        if (!key2.empty())
        {
            writer.Key(StringRef(keys[1]), keys[1].size());
            writer.String(StringRef(key2));
        }
        writer.EndObject();
    }

    writer.EndArray();
    writer.EndObject();

    return 200;
}
