#include "db.h"

using namespace std;

size_t Db::likes(const Document &doc) noexcept
{
    const auto &likes_array = doc["likes"];
    for (const auto &like : likes_array.GetArray())
    {
        if (!like.HasMember("liker") || !like.HasMember("likee") || !like.HasMember("ts") || !like["liker"].IsInt() || !like["likee"].IsInt() || !like["ts"].IsInt())
        {
            return 400;
        }

        auto liker = like["liker"].GetInt();
        auto likee = like["likee"].GetInt();
        if (!account_exist(liker) || !account_exist(likee))
        {
            return 400;
        }
    }

    for (const auto &like : likes_array.GetArray())
    {
        auto liker = like["liker"].GetInt();
        auto likee = like["likee"].GetInt();
        auto ts = like["ts"].GetInt();

        auto &a = accounts[liker];
        a.add_like(likee, ts);
        // liked_index[likee].push_back(liker);
        liked_sex_index[likee][a.sex].push_back(liker);
    }

    return 202;
};
