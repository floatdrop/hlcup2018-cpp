#include "db.h"

using namespace std;

inline uint8_t common_interests(const Account &target, const Account &candidate) noexcept
{
    return (target.interests_bits & candidate.interests_bits).count();
}

size_t Db::recommend(const uint32_t id, const SimpleWeb::CaseInsensitiveMultimap &query, Writer<StringBuffer> &writer) noexcept
{
    auto limit_iter = query.find("limit");
    if (limit_iter == query.end())
    {
        return 400;
    }
    size_t limit;

    if (!absl::SimpleAtoi(limit_iter->second, &limit) || limit < 1 || limit > 20)
    {
        return 400;
    };

    const auto &target = accounts[id];

    std::array<std::vector<Candidate>, 16> buckets;
    buckets[4].reserve(16);
    buckets[3].reserve(16);
    buckets[2].reserve(128);
    buckets[1].reserve(1024);
    buckets[0].reserve(8096);

    uint16_t city_filter_value = 0;
    uint8_t country_filter_value = 0;

    for (const auto &[key, value] : query)
    {
        if (key == "limit" || key == "query_id")
        {
            continue;
        }

        if (key == "country")
        {
            if (value == "")
            {
                return 400;
            }
            country_filter_value = Db::countries_dict[value];
        }
        else if (key == "city")
        {
            if (value == "")
            {
                return 400;
            }
            city_filter_value = Db::cities_dict[value];
        }
        else
        {
            return 400;
        }
    }

    writer.StartObject();
    writer.Key("accounts");
    writer.StartArray();

    if (target.interests_bits.count() == 0)
    {
        goto finish;
    }

    for (auto premium : {1, 0})
    {
        for (auto status : {1, 2, 3})
        {
            for (auto &bucket : buckets)
            {
                bucket.clear();
            }
            absl::flat_hash_set<uint32_t> seen_cids;
            for (size_t i = 0; i < 128; ++i)
            {
                if (!target.interests_bits.test(i))
                {
                    continue;
                }

                for (auto cid : premium_sex_status_interest_index[{premium, !target.sex, status, i}])
                {
                    const auto &candidate = accounts[cid];

                    if (city_filter_value != 0 && candidate.city != city_filter_value)
                    {
                        continue;
                    }

                    if (country_filter_value != 0 && candidate.country != country_filter_value)
                    {
                        continue;
                    }

                    auto ci = common_interests(target, candidate);
                    buckets[ci - 1].emplace_back(std::abs(candidate.birth - target.birth), cid);
                }
            }

            for (int b = buckets.size() - 1; b >= 0; b--)
            {
                if (buckets[b].size() == 0)
                {
                    continue;
                }

                auto &bucket = buckets[b];
                std::partial_sort(bucket.begin(), bucket.begin() + min(limit * target.interests_bits.count(), bucket.size()), bucket.end());

                uint32_t last_id = 400000000;

                for (const auto &candidate : bucket)
                {
                    if (last_id == candidate.id)
                    {
                        continue;
                    }

                    last_id = candidate.id;
                    const auto id = candidate.id;
                    limit--;

                    auto &account = accounts[id];
                    writer.StartObject();
                    writer.Key("id");
                    writer.Uint(id);
                    writer.Key("email");
                    writer.String(StringRef(account.email));
                    writer.Key("status");
                    writer.String(Db::statuses[account.status]);

                    if (account.fname != 0)
                    {
                        writer.Key("fname");
                        writer.String(StringRef(Db::fnames_dict[account.fname]));
                    }

                    if (account.sname != 0)
                    {
                        writer.Key("sname");
                        writer.String(StringRef(Db::snames_dict[account.sname]));
                    }

                    writer.Key("birth");
                    writer.Uint(account.birth);

                    if (account.premium_start != 0)
                    {
                        writer.Key("premium");
                        writer.StartObject();
                        writer.Key("start");
                        writer.Uint(account.premium_start);
                        writer.Key("finish");
                        writer.Uint(account.premium_finish);
                        writer.EndObject();
                    }

                    writer.EndObject();

                    if (limit == 0)
                    {
                        goto finish;
                    }
                }
            }
        }
    }

finish:
    writer.EndArray();
    writer.EndObject();

    return 200;
}
