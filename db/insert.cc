#include "db.h"

using namespace std;

size_t Db::insert(const Document &account) noexcept
{
    if (!account.HasMember("id") || !account.HasMember("email") || !account.HasMember("sex") || !account.HasMember("status") || !account.HasMember("birth") || !account.HasMember("joined"))
    {
        return 400;
    }

    uint32_t id = account["id"].GetInt();

    string email = account["email"].GetString();
    if (email_tree.find(email) != email_tree.end())
    {
        return 400;
    }

    if (email.find_first_of('@') == string::npos)
    {
        return 400;
    }

    Account a;
    a.email = email;
    a.email_domain = domain_dict[parse_email_domain(email)];
    a.premium_now = false;

    std::string_view sex = account["sex"].GetString();
    if (sex != "m" && sex != "f")
    {
        return 400;
    }

    a.sex = parse_sex(sex);
    a.status = parse_status(account["status"].GetString());
    if (a.status == 0)
    {
        return 4000;
    }
    a.city = account.HasMember("city") ? cities_dict[account["city"].GetString()] : 0;
    a.country = account.HasMember("country") ? countries_dict[account["country"].GetString()] : 0;
    a.birth = account["birth"].GetInt();
    a.joined = account["joined"].GetInt();

    if (account.HasMember("phone"))
    {
        a.phone = account["phone"].GetString();
        uint32_t pcode;
        if (!absl::SimpleAtoi(a.phone.substr(2, 3), &pcode))
        {
            return 400;
        };
        a.pcode = pcode;
    }
    else
    {
        a.pcode = 0;
    }

    if (account.HasMember("fname"))
    {
        auto fname = account["fname"].GetString();
        a.fname = account.HasMember("fname") ? fnames_dict[fname] : 0;
        sex_names[a.sex].insert(fname);
    }
    else
    {
        a.fname = 0;
    }

    a.sname = account.HasMember("sname") ? snames_dict[account["sname"].GetString()] : 0;

    if (a.sname != 0)
    {
        sname_tree.insert(snames_dict[a.sname]);
    }

    if (account.HasMember("interests"))
    {
        auto array = account["interests"].GetArray();
        for (const auto &v : array)
        {
            a.interests_bits.set(interests_dict[v.GetString()]);
        }
    }

    if (account.HasMember("likes"))
    {
        auto array = account["likes"].GetArray();
        a.likes.reserve(array.Size());

        for (const auto &v : array)
        {
            auto likee = v["id"].GetInt();
            auto ts = v["ts"].GetInt();
            a.add_like(likee, ts);
            // liked_index[likee].push_back(id);
            liked_sex_index[likee][a.sex].push_back(id);
        }
    }

    if (account.HasMember("premium"))
    {
        if (!account["premium"].IsObject() || !account["premium"].HasMember("start") || !account["premium"].HasMember("finish"))
        {
            return 400;
        }

        a.premium_start = account["premium"]["start"].GetInt();
        a.premium_finish = account["premium"]["finish"].GetInt();
        if (a.premium_start != 0 && a.premium_finish != 0 && a.premium_start < Db::current_time && Db::current_time < a.premium_finish)
        {
            a.premium_now = true;
        }
    }
    else
    {
        a.premium_start = 0;
        a.premium_finish = 0;
        a.premium_now = false;
    }

    // Update primary indexes
    email_tree.insert(email);

    accounts[id] = a;

    return 202;
};
