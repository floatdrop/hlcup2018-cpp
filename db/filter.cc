#include "db.h"

using namespace std;

#define MAX_INDEX_SIZE 100000
#define MAX_INDEX_GAP_FACTOR 2

struct Range
{
    uint32_t value;
    id_set::iterator begin;
    id_set::iterator end;

    Range(id_set::iterator b, id_set::iterator e) : begin(b), end(e)
    {
        value = *begin;
    }

    bool operator<(const Range &y) const noexcept
    {
        return value < y.value;
    };
};

void heap_union(std::vector<id_set *> &ranges, std::function<bool(uint32_t id)> f)
{
    if (ranges.size() == 1)
    {
        auto end = ranges[0]->end();
        for (auto i = ranges[0]->begin(); i != end; ++i)
        {
            if (!f(*i))
            {
                return;
            }
        }
        return;
    }

    std::priority_queue<Range> heap;

    for (auto range_ptr : ranges)
    {
        if (!range_ptr->empty())
        {
            heap.emplace(range_ptr->begin(), range_ptr->end());
        }
    }

    uint32_t last_value = 4000000;

    while (heap.size() > 0)
    {
        Range range = heap.top();
        heap.pop();

        if (range.value != last_value)
        {
            last_value = range.value;
            if (!f(range.value))
            {
                return;
            }
        }

        std::advance(range.begin, 1);

        if (range.begin != range.end)
        {
            heap.emplace(range.begin, range.end);
        }
    }
};

id_set likes_intersect(id_set *a, id_set *b) noexcept
{
    id_set result;
    result.reserve(a->size() / 2);
    std::set_intersection(a->begin(), a->end(),
                          b->begin(), b->end(),
                          std::back_insert_iterator(result),
                          std::greater<uint32_t>());
    return result;
}

size_t get_size(std::vector<id_set *> &indexes)
{
    size_t size = 0;
    for (auto &idx : indexes)
    {
        size += idx->size();
    }
    return size;
}

size_t Db::filter(const SimpleWeb::CaseInsensitiveMultimap &query, Writer<StringBuffer> &writer) noexcept
{
    auto limit_iter = query.find("limit");
    if (limit_iter == query.end())
    {
        return 400;
    }

    int32_t limit;
    if (!absl::SimpleAtoi(limit_iter->second, &limit) || limit < 1 || limit > 50)
    {
        return 400;
    };

    bool response_has_sex = false;
    bool response_has_status = false;
    bool response_has_city = false;
    bool response_has_country = false;
    bool response_has_fname = false;
    bool response_has_sname = false;
    bool response_has_phone = false;
    bool response_has_birth = false;
    bool response_has_premium = false;

    vector<std::unique_ptr<Predicate>> filters;

    id_set intersection;
    std::vector<id_set *> index = {&ids_index};

    absl::flat_hash_map<std::string_view, std::string_view> q;

    for (const auto &[key, value] : query)
    {
        if (!Db::allowed_filter_keys.contains(key))
        {
            return 400;
        }
        q[key] = value;
    }

    if (q.contains("email_domain"))
    {
        auto &value = q["email_domain"];
        if (domain_index[domain_dict[value]].size() < get_size(index))
        {
            index = {&domain_index[domain_dict[value]]};
        }
        filters.push_back(std::make_unique<EmailDomainEq>(domain_dict[value]));
    }

    if (q.contains("email_lt"))
    {
        auto &value = q["email_lt"];
        auto begin = email_prefixes.begin();
        auto end = std::upper_bound(email_prefixes.begin(), email_prefixes.end(), value.substr(0, 2));

        auto size = 0;
        for (auto i = begin; i != end; ++i)
        {
            size += email_prefix_index[*i].size();
        }

        if (size < get_size(index))
        {
            index.clear();
            for (auto i = begin; i != end; ++i)
            {
                index.push_back(&email_prefix_index[*i]);
            }
        }

        filters.push_back(std::make_unique<EmailLt>(value));
    }

    if (q.contains("email_gt"))
    {
        auto &value = q["email_gt"];
        auto begin = std::lower_bound(email_prefixes.begin(), email_prefixes.end(), value.substr(0, 2));
        auto end = email_prefixes.end();

        auto size = 0;
        for (auto i = begin; i != end; ++i)
        {
            size += email_prefix_index[*i].size();
        }

        if (size < get_size(index))
        {
            index.clear();
            for (auto i = begin; i != end; ++i)
            {
                index.push_back(&email_prefix_index[*i]);
            }
        }

        filters.push_back(std::make_unique<EmailGt>(value));
    }

    if (q.contains("fname_eq"))
    {
        auto &value = q["fname_eq"];
        response_has_fname = true;
        auto id = Db::fnames_dict[value];
        if (fname_index[id].size() < get_size(index))
        {
            index = {&fname_index[id]};
        }
        filters.push_back(std::make_unique<FnameEq>(value));
    }

    if (q.contains("fname_null"))
    {
        auto &value = q["fname_null"];
        if (value == "0")
        {
            response_has_fname = true;
            //                                 1182380
            // printf("fname_nnull_index size\t%d\n", fname_nnull_index.size());
            if (fname_nnull_index.size() < get_size(index))
            {
                index = {&fname_nnull_index};
            }
            filters.push_back(std::make_unique<FnameNotNull>());
        }
        else if (value == "1")
        {
            response_has_fname = false;
            // printf("fname_index size\t%d\n", fname_index[0].size());
            if (fname_index[0].size() < get_size(index))
            {
                index = {&fname_index[0]};
            }
            filters.push_back(std::make_unique<FnameNull>());
        }
    }

    if (q.contains("sname_eq"))
    {
        auto &value = q["sname_eq"];
        response_has_sname = true;
        auto id = snames_dict[value];
        if (sname_index[id].size() < get_size(index))
        {
            index = {&sname_index[id]};
        }
        filters.push_back(std::make_unique<SnameEq>(value));
    }

    if (q.contains("sname_null"))
    {
        auto &value = q["sname_null"];
        if (value == "0")
        {
            response_has_sname = true;
            // printf("sname_nnull_index size\t%d\n", sname_nnull_index.size());
            if (sname_nnull_index.size() < get_size(index))
            {
                index = {&sname_nnull_index};
            }
            filters.push_back(std::make_unique<SnameNotNull>());
        }
        else if (value == "1")
        {
            response_has_sname = false;
            // printf("sname_index size\t%d\n", sname_index[0].size());
            if (sname_index[0].size() < get_size(index))
            {
                index = {&sname_index[0]};
            }
            filters.push_back(std::make_unique<SnameNull>());
        }
    }

    if (q.contains("phone_code"))
    {
        auto &value = q["phone_code"];
        uint32_t code;
        if (!absl::SimpleAtoi(value, &code))
        {
            return 400;
        }

        response_has_phone = true;
        if (phone_index[code].size() < get_size(index))
        {
            index = {&phone_index[code]};
        }
        filters.push_back(std::make_unique<PhoneCode>(code));
    }

    if (q.contains("phone_null"))
    {
        auto &value = q["phone_null"];
        if (value == "0")
        {
            response_has_phone = true;
            // printf("phone_nnull size\t%d\n", phone_nnull_index.size());
            if (phone_nnull_index.size() < get_size(index))
            {
                index = {&phone_nnull_index};
            }
            filters.push_back(std::make_unique<PhoneNotNull>());
        }
        else if (value == "1")
        {
            response_has_phone = false;
            // printf("phone_null size\t%d\n", phone_index[0].size());
            if (phone_index[0].size() < get_size(index))
            {
                index = {&phone_index[0]};
            }
            filters.push_back(std::make_unique<PhoneNull>());
        }
    }

    if (q.contains("birth_year"))
    {
        auto &value = q["birth_year"];
        response_has_birth = true;
        uint32_t year;
        if (!absl::SimpleAtoi(value, &year))
        {
            return 400;
        }
        if (birth_index[year].size() < get_size(index))
        {
            index = {&birth_index[year]};
        }
        filters.push_back(std::make_unique<BirthYear>(year));
    }

    if (q.contains("birth_lt"))
    {
        auto &value = q["birth_lt"];
        response_has_birth = true;
        uint32_t birth;
        if (!absl::SimpleAtoi(value, &birth))
        {
            return 400;
        }
        uint32_t birth_year = year_from_ts(birth);

        auto size = 0;
        for (auto &[year, idx] : birth_index)
        {
            if (year <= birth_year)
            {
                size += idx.size();
            }
        }

        if (size < get_size(index))
        {
            index.clear();
            for (auto &[year, idx] : birth_index)
            {
                if (year <= birth_year)
                {
                    index.push_back(&idx);
                }
            }
        }

        filters.push_back(std::make_unique<BirthLt>(birth));
    }

    if (q.contains("birth_gt"))
    {
        auto &value = q["birth_gt"];
        response_has_birth = true;
        uint32_t birth;
        if (!absl::SimpleAtoi(value, &birth))
        {
            return 400;
        }
        uint32_t birth_year = year_from_ts(birth);

        auto size = 0;
        for (auto &[year, idx] : birth_index)
        {
            if (year >= birth_year)
            {
                size += idx.size();
            }
        }

        if (size < get_size(index))
        {
            index.clear();
            for (auto &[year, idx] : birth_index)
            {
                if (year >= birth_year)
                {
                    index.push_back(&idx);
                }
            }
        }

        filters.push_back(std::make_unique<BirthGt>(birth));
    }

    if (q.contains("premium_now"))
    {
        auto &value = q["premium_now"];
        response_has_premium = true;
        if (value == "0")
        {
            // Not used in filters
            filters.push_back(std::make_unique<PremiumNotNow>());
        }
        else if (value == "1")
        {
            // printf("premium_now size\t%d\n", premium_now_index.size());
            if (premium_now_index.size() < get_size(index))
            {
                index = {&premium_now_index};
            }
            filters.push_back(std::make_unique<PremiumNow>());
        }
    }

    if (q.contains("premium_null"))
    {
        auto &value = q["premium_null"];
        if (value == "0")
        {
            response_has_premium = true;
            // printf("premium_nnull size\t%d\n", premium_nnull_index.size());
            if (premium_nnull_index.size() < get_size(index))
            {
                index = {&premium_nnull_index};
            }
            filters.push_back(std::make_unique<PremiumNotNull>());
        }
        else if (value == "1")
        {
            response_has_premium = false;
            // printf("premium_null size\t%d\n", premium_null_index.size());
            if (premium_null_index.size() < get_size(index))
            {
                index = {&premium_null_index};
            }
            filters.push_back(std::make_unique<PremiumNull>());
        }
    }

    if (q.contains("country_eq"))
    {
        auto &value = q["country_eq"];
        response_has_country = true;
        auto id = countries_dict[value];

        if (q.contains("sex_eq") && q.contains("status_eq"))
        {
            auto sex = parse_sex(q["sex_eq"]);
            auto status = parse_status(q["status_eq"]);
            if (country_sex_status_index[{(uint8_t)id, sex, status}].size() < get_size(index))
            {
                index = {&country_sex_status_index[{(uint8_t)id, sex, status}]};
            }
        }
        else if (q.contains("sex_eq"))
        {
            auto sex = parse_sex(q["sex_eq"]);
            auto size = country_sex_status_index[{(uint8_t)id, sex, 1}].size() + country_sex_status_index[{(uint8_t)id, sex, 2}].size() + country_sex_status_index[{(uint8_t)id, sex, 3}].size();
            if (size < get_size(index))
            {
                index = {
                    &country_sex_status_index[{(uint8_t)id, sex, 1}],
                    &country_sex_status_index[{(uint8_t)id, sex, 2}],
                    &country_sex_status_index[{(uint8_t)id, sex, 3}]};
            }
        }
        else if (q.contains("status_eq"))
        {
            auto status = parse_status(q["status_eq"]);
            auto size = country_sex_status_index[{(uint8_t)id, false, status}].size() + country_sex_status_index[{(uint8_t)id, true, status}].size();
            if (size < get_size(index))
            {
                index = {
                    &country_sex_status_index[{(uint8_t)id, false, status}],
                    &country_sex_status_index[{(uint8_t)id, true, status}]};
            }
        }

        if (country_index[id].size() < get_size(index))
        {
            index = {&country_index[id]};
        }

        filters.push_back(std::make_unique<CountryEq>(id));
    }

    if (q.contains("country_null"))
    {
        auto &value = q["country_null"];
        if (value == "0")
        {
            response_has_country = true;
            // printf("country_nnull size\t%d\n", country_nnull_index.size());
            if (country_nnull_index.size() < get_size(index))
            {
                index = {&country_nnull_index};
            }
            filters.push_back(std::make_unique<CountryNotNull>());
        }
        else if (value == "1")
        {
            response_has_country = false;
            if (q.contains("status_eq") && q.contains("sex_eq"))
            {
                auto sex = parse_sex(q["sex_eq"]);
                auto status = parse_status(q["status_eq"]);
                auto size = country_sex_status_index[{0, sex, status}].size();
                if (size < get_size(index))
                {
                    index.push_back(&country_sex_status_index[{0, sex, status}]);
                }
            }
            else if (q.contains("status_eq"))
            {
                auto status = parse_status(q["status_eq"]);
                auto size = country_sex_status_index[{0, false, status}].size() + country_sex_status_index[{0, true, status}].size();
                if (size < get_size(index))
                {
                    index.push_back(&country_sex_status_index[{0, false, status}]);
                    index.push_back(&country_sex_status_index[{0, true, status}]);
                }
            }
            else if (q.contains("sex_eq"))
            {
                auto sex = parse_sex(q["sex_eq"]);
                auto size = country_sex_status_index[{0, sex, 1}].size() + country_sex_status_index[{0, sex, 2}].size() + country_sex_status_index[{0, sex, 3}].size();
                if (size < get_size(index))
                {
                    index.push_back(&country_sex_status_index[{0, sex, 1}]);
                    index.push_back(&country_sex_status_index[{0, sex, 2}]);
                    index.push_back(&country_sex_status_index[{0, sex, 3}]);
                }
            }
            else if (country_index[0].size() < get_size(index))
            {
                index = {&country_index[0]};
            }
            filters.push_back(std::make_unique<CountryNull>());
        }
    }

    if (q.contains("city_eq"))
    {
        auto &value = q["city_eq"];
        response_has_city = true;
        auto id = cities_dict[value];

        if (q.contains("sex_eq") && q.contains("status_eq"))
        {
            auto sex = parse_sex(q["sex_eq"]);
            auto status = parse_status(q["status_eq"]);
            if (city_sex_status_index[{(uint16_t)id, sex, status}].size() < get_size(index))
            {
                index = {&city_sex_status_index[{(uint16_t)id, sex, status}]};
            }
        }
        else if (q.contains("sex_eq"))
        {
            auto sex = parse_sex(q["sex_eq"]);
            auto size = city_sex_status_index[{(uint16_t)id, sex, 1}].size() + city_sex_status_index[{(uint16_t)id, sex, 2}].size() + city_sex_status_index[{(uint16_t)id, sex, 3}].size();
            if (size < get_size(index))
            {
                index = {
                    &city_sex_status_index[{(uint16_t)id, sex, 1}],
                    &city_sex_status_index[{(uint16_t)id, sex, 2}],
                    &city_sex_status_index[{(uint16_t)id, sex, 3}]};
            }
        }
        else if (q.contains("status_eq"))
        {
            auto status = parse_status(q["status_eq"]);
            auto size = city_sex_status_index[{(uint16_t)id, false, status}].size() + city_sex_status_index[{(uint16_t)id, true, status}].size();
            if (size < get_size(index))
            {
                index = {
                    &city_sex_status_index[{(uint16_t)id, false, status}],
                    &city_sex_status_index[{(uint16_t)id, true, status}]};
            }
        }

        if (city_index[id].size() < get_size(index))
        {
            index = {&city_index[id]};
        }
        filters.push_back(std::make_unique<CityEq>(id));
    }

    if (q.contains("city_null"))
    {
        auto &value = q["city_null"];
        if (value == "0")
        {
            response_has_city = true;
            // printf("city_nnull size\t%d\n", city_nnull_index.size());
            if (city_nnull_index.size() < get_size(index))
            {
                index = {&city_nnull_index};
            }
            filters.push_back(std::make_unique<CityNotNull>());
        }
        else if (value == "1")
        {
            response_has_city = false;

            if (q.contains("status_eq") && q.contains("sex_eq"))
            {
                auto sex = parse_sex(q["sex_eq"]);
                auto status = parse_status(q["status_eq"]);
                auto size = city_sex_status_index[{0, sex, status}].size();
                if (size < get_size(index))
                {
                    index.push_back(&city_sex_status_index[{0, sex, status}]);
                }
            }
            else if (q.contains("status_eq"))
            {
                auto status = parse_status(q["status_eq"]);
                auto size = city_sex_status_index[{0, false, status}].size() + city_sex_status_index[{0, true, status}].size();
                if (size < get_size(index))
                {
                    index.push_back(&city_sex_status_index[{0, false, status}]);
                    index.push_back(&city_sex_status_index[{0, true, status}]);
                }
            }
            else if (q.contains("sex_eq"))
            {
                auto sex = parse_sex(q["sex_eq"]);
                auto size = city_sex_status_index[{0, sex, 1}].size() + city_sex_status_index[{0, sex, 2}].size() + city_sex_status_index[{0, sex, 3}].size();
                if (size < get_size(index))
                {
                    index.push_back(&city_sex_status_index[{0, sex, 1}]);
                    index.push_back(&city_sex_status_index[{0, sex, 2}]);
                    index.push_back(&city_sex_status_index[{0, sex, 3}]);
                }
            }
            else if (city_index[0].size() < get_size(index))
            {
                index = {&city_index[0]};
            }
            filters.push_back(std::make_unique<CityNull>());
        }
    }

    if (q.contains("interests_contains"))
    {
        auto &value = q["interests_contains"];

        std::vector<std::string_view> interests = absl::StrSplit(value, ',');

        if (interests.size() == 1)
        {
            if (get_size(index) > interested_index[Db::interests_dict[interests[0]]].size())
            {
                index = {&interested_index[Db::interests_dict[interests[0]]]};
            }
        }
        else if (interests.size() == 2)
        {
            auto a = Db::interests_dict[interests[0]];
            auto b = Db::interests_dict[interests[1]];
            if (a > b)
            {
                std::swap(a, b);
            }

            if (get_size(index) > interested_2_index[{a, b}].size())
            {
                index = {&interested_2_index[{a, b}]};
            }
        }
        else if (interests.size() == 3)
        {
            auto a = Db::interests_dict[interests[0]];
            auto b = Db::interests_dict[interests[1]];
            if (a > b)
            {
                std::swap(a, b);
            }

            auto c = Db::interests_dict[interests[2]];

            auto first = interested_2_index[{a, b}];
            auto second = interested_index[c];

            id_set result;
            std::set_intersection(first.begin(), first.end(), second.begin(), second.end(), std::back_inserter(result), std::greater<uint32_t>());

            if (result.size() < get_size(index))
            {
                intersection = std::move(result);
                index = {&intersection};
            }
        }
        else if (interests.size() == 4)
        {
            auto a = Db::interests_dict[interests[0]];
            auto b = Db::interests_dict[interests[1]];
            if (a > b)
            {
                std::swap(a, b);
            }
            auto c = Db::interests_dict[interests[1]];
            auto d = Db::interests_dict[interests[2]];
            if (c > d)
            {
                std::swap(c, d);
            }
            auto first = interested_2_index[{a, b}];
            auto second = interested_2_index[{c, d}];

            id_set result;
            std::set_intersection(first.begin(), first.end(), second.begin(), second.end(), std::back_inserter(result), std::greater<uint32_t>());

            if (result.size() < get_size(index))
            {
                intersection = std::move(result);
                index = {&intersection};
            }
        }
        else if (interests.size() == 5)
        {
            std::vector<size_t> ints = {Db::interests_dict[interests[0]], Db::interests_dict[interests[1]], Db::interests_dict[interests[2]], Db::interests_dict[interests[3]], Db::interests_dict[interests[4]]};
            std::sort(ints.begin(), ints.end());
            if (get_size(index) > interested_5_index[{ints[0], ints[1], ints[2], ints[3], ints[4]}].size())
            {
                index = {&interested_5_index[{ints[0], ints[1], ints[2], ints[3], ints[4]}]};
            }
        }

        filters.push_back(std::make_unique<InterestsContains>(value));
    }

    if (q.contains("likes_contains"))
    {
        auto &value = q["likes_contains"];
        
        std::vector<std::string_view> likes = absl::StrSplit(value, ',');

        if (likes.size() == 1) {
            uint32_t l;
            if (!absl::SimpleAtoi(likes[0], &l))
            {
                return 400;
            }

            if (q.contains("sex_eq")) {
                auto sex = parse_sex(q["sex_eq"]);
                if (liked_sex_index[l][sex].size() < get_size(index)) {
                    index = {&liked_sex_index[l][sex]};
                }
            } else {
                if (liked_sex_index[l][false].size() + liked_sex_index[l][true].size() < get_size(index)) {
                    index = {&liked_sex_index[l][false], &liked_sex_index[l][true]};
                }
            }
        } else if (likes.size() == 2) {
            uint32_t a;
            if (!absl::SimpleAtoi(likes[0], &a))
            {
                return 400;
            }
            
            uint32_t b;
            if (!absl::SimpleAtoi(likes[1], &b))
            {
                return 400;
            }

            if (q.contains("sex_eq")) {
                auto sex = parse_sex(q["sex_eq"]);
                id_set result;
                std::set_intersection(liked_sex_index[a][sex].begin(), liked_sex_index[a][sex].end(), liked_sex_index[b][sex].begin(), liked_sex_index[b][sex].end(), std::back_inserter(result));
                if (result.size() < get_size(index)) {
                    intersection = std::move(result);
                    index = {&intersection};
                }
            } else {
                id_set result;
                std::set_intersection(liked_sex_index[a][false].begin(), liked_sex_index[a][false].end(),
                                      liked_sex_index[b][false].begin(), liked_sex_index[b][false].end(), std::back_inserter(result));

                auto dist = result.size();

                std::set_intersection(liked_sex_index[a][true].begin(), liked_sex_index[a][true].end(),
                                      liked_sex_index[b][true].begin(), liked_sex_index[b][true].end(), std::back_inserter(result));
                
                if (result.size() < get_size(index)) {
                    std::inplace_merge(result.begin(), result.begin() + dist, result.end());
                    intersection = std::move(result);
                    index = {&intersection};
                }
            }
        } else if (likes.size() == 3) {
            uint32_t a;
            if (!absl::SimpleAtoi(likes[0], &a))
            {
                return 400;
            }
            
            uint32_t b;
            if (!absl::SimpleAtoi(likes[1], &b))
            {
                return 400;
            }

            uint32_t c;
            if (!absl::SimpleAtoi(likes[2], &c))
            {
                return 400;
            }

            if (q.contains("sex_eq")) {
                auto sex = parse_sex(q["sex_eq"]);
                id_set result;
                std::set_intersection(liked_sex_index[a][sex].begin(), liked_sex_index[a][sex].end(), liked_sex_index[b][sex].begin(), liked_sex_index[b][sex].end(), std::back_inserter(result));
                auto e = std::set_intersection(result.begin(), result.end(), liked_sex_index[c][sex].begin(), liked_sex_index[c][sex].end(), result.begin());
                result.resize(std::distance(result.begin(), e));
                if (result.size() < get_size(index)) {
                    intersection = std::move(result);
                    index = {&intersection};
                }
            } else {
                id_set result;
                std::set_intersection(liked_sex_index[a][false].begin(), liked_sex_index[a][false].end(),
                                      liked_sex_index[b][false].begin(), liked_sex_index[b][false].end(), std::back_inserter(result));
                auto e = std::set_intersection(result.begin(), result.end(), liked_sex_index[c][false].begin(), liked_sex_index[c][false].end(), result.begin());
                result.resize(std::distance(result.begin(), e));

                auto dist = result.size();

                std::set_intersection(liked_sex_index[a][true].begin(), liked_sex_index[a][true].end(),
                                      liked_sex_index[b][true].begin(), liked_sex_index[b][true].end(), std::back_inserter(result));
                e = std::set_intersection(result.begin() + dist, result.end(), liked_sex_index[c][true].begin(), liked_sex_index[c][true].end(), result.begin() + dist);
                result.resize(std::distance(result.begin(), e));

                if (result.size() < get_size(index)) {
                    std::inplace_merge(result.begin(), result.begin() + dist, result.end());
                    intersection = std::move(result);
                    index = {&intersection};
                }
            }
        }

        filters.push_back(std::make_unique<LikesContains>(value));
    }

    // Union indexes

    if (q.contains("city_any") && q.contains("status_eq") && q.contains("sex_eq"))
    {
        auto sex = parse_sex(q["sex_eq"]);
        auto status = parse_status(q["status_eq"]);
        auto &value = q["city_any"];
        response_has_city = true;
        auto size = 0;
        std::vector<std::string_view> cities = absl::StrSplit(value, ',');
        for (auto city : cities)
        {
            auto id = cities_dict[city];
            size += city_sex_status_index[{id, sex, status}].size();
        }

        if (size < get_size(index))
        {
            index.clear();
            for (auto city : cities)
            {
                auto id = cities_dict[city];
                index.push_back(&city_sex_status_index[{id, sex, status}]);
            }
        }

        filters.push_back(std::make_unique<CityAny>(value));
    }
    else if (q.contains("city_any") && q.contains("sex_eq"))
    {
        auto sex = parse_sex(q["sex_eq"]);
        auto &value = q["city_any"];
        response_has_city = true;
        auto size = 0;
        std::vector<std::string_view> cities = absl::StrSplit(value, ',');
        for (auto city : cities)
        {
            auto id = cities_dict[city];
            size += city_sex_status_index[{id, sex, 1}].size();
            size += city_sex_status_index[{id, sex, 2}].size();
            size += city_sex_status_index[{id, sex, 3}].size();
        }

        if (size < get_size(index))
        {
            index.clear();
            for (auto city : cities)
            {
                auto id = cities_dict[city];
                index.push_back(&city_sex_status_index[{id, sex, 1}]);
                index.push_back(&city_sex_status_index[{id, sex, 2}]);
                index.push_back(&city_sex_status_index[{id, sex, 3}]);
            }
        }

        filters.push_back(std::make_unique<CityAny>(value));
    }
    else if (q.contains("city_any") && q.contains("status_eq"))
    {
        auto status = parse_status(q["status_eq"]);
        auto &value = q["city_any"];
        response_has_city = true;
        auto size = 0;
        std::vector<std::string_view> cities = absl::StrSplit(value, ',');
        for (auto city : cities)
        {
            auto id = cities_dict[city];
            size += city_sex_status_index[{id, false, status}].size();
            size += city_sex_status_index[{id, true, status}].size();
        }

        if (size < get_size(index))
        {
            index.clear();
            for (auto city : cities)
            {
                auto id = cities_dict[city];
                index.push_back(&city_sex_status_index[{id, false, status}]);
                index.push_back(&city_sex_status_index[{id, true, status}]);
            }
        }

        filters.push_back(std::make_unique<CityAny>(value));
    }
    else if (q.contains("city_any"))
    {
        auto &value = q["city_any"];
        response_has_city = true;
        auto size = 0;
        std::vector<std::string_view> cities = absl::StrSplit(value, ',');
        for (auto city : cities)
        {
            auto id = cities_dict[city];
            size += city_index[id].size();
        }

        if (size < get_size(index))
        {
            index.clear();
            for (auto city : cities)
            {
                auto id = cities_dict[city];
                index.push_back(&city_index[id]);
            }
        }

        filters.push_back(std::make_unique<CityAny>(value));
    }

    if (q.contains("sname_starts"))
    {
        auto &value = q["sname_starts"];
        response_has_sname = true;

        auto prefix_range = sname_tree.equal_prefix_range(value);
        auto size = 0;
        for (auto it = prefix_range.first; it != prefix_range.second; ++it)
        {
            size += sname_index[snames_dict[it.key()]].size();
        }

        if (size < get_size(index))
        {
            index.clear();
            for (auto it = prefix_range.first; it != prefix_range.second; ++it)
            {
                index.push_back(&sname_index[snames_dict[it.key()]]);
            }
        }

        filters.push_back(std::make_unique<SnameStarts>(value));
    }

    if (q.contains("fname_any"))
    {
        auto &value = q["fname_any"];
        response_has_fname = true;
        // if (indexes.size() < 2)
        // {
        bool has_sex_eq = false;
        bool sex;
        if (q.contains("sex_eq"))
        {
            has_sex_eq = true;
            auto &value = q["sex_eq"];
            sex = parse_sex(value);
        }

        std::vector<string_view> fnames = absl::StrSplit(value, ',');

        auto size = 0;
        for (auto fname : fnames)
        {
            if (has_sex_eq && sex_names[sex].find(fname) == sex_names[sex].end())
            {
                continue;
            }
            size += fname_index[fnames_dict[fname]].size();
        }

        if (size < get_size(index))
        {
            index.clear();
            for (auto fname : fnames)
            {
                if (has_sex_eq && sex_names[sex].find(fname) == sex_names[sex].end())
                {
                    continue;
                }
                index.push_back(&fname_index[fnames_dict[fname]]);
            }
        }

        filters.push_back(std::make_unique<FnameAny>(value));
    }

    if (q.contains("interests_any"))
    {
        auto &value = q["interests_any"];
        auto size = 0;
        std::vector<std::string_view> interests = absl::StrSplit(value, ',');

        for (auto interest : interests)
        {
            size += interested_index[interests_dict[interest]].size();
        }

        if (size < get_size(index))
        {
            index.clear();
            for (auto interest : interests)
            {
                index.push_back(&interested_index[interests_dict[interest]]);
            }
        }

        filters.push_back(std::make_unique<InterestsAny>(value));
    }

    if (q.contains("sex_eq"))
    {
        auto &value = q["sex_eq"];
        response_has_sex = true;
        const auto sex = parse_sex(value);
        if (sex_index[sex].size() < get_size(index))
        {
            index = {&sex_index[sex]};
        }

        filters.push_back(std::make_unique<SexEq>(sex));
    }

    if (q.contains("status_eq"))
    {
        auto &value = q["status_eq"];
        response_has_status = true;
        const auto status = parse_status(value);
        if (status_index[status].size() < get_size(index))
        {
            index = {&status_index[status]};
        }
        filters.push_back(std::make_unique<StatusEq>(status));
    }

    if (q.contains("status_neq"))
    {
        auto &value = q["status_neq"];
        response_has_status = true;
        const auto status = parse_status(value);
        if (status_neq_index[status].size() < get_size(index))
        {
            index = {&status_neq_index[status]};
        }
        filters.push_back(std::make_unique<StatusNeq>(status));
    }

    writer.StartObject();
    writer.Key("accounts");
    writer.StartArray();

    size_t found_accounts = 0;
    auto checked_accounts = 0;

    // printf("Index: %d\n", get_size(index));

    heap_union(index, [&](uint32_t id) {
        checked_accounts++;
        auto &account = accounts[id];

        if (!match_filters(filters, account))
        {
            return true;
        }

        found_accounts++;

        writer.StartObject();
        writer.Key("id");
        writer.Uint(id);
        writer.Key("email");
        writer.String(StringRef(account.email));

        if (response_has_sex)
        {
            writer.Key("sex");
            writer.String(StringRef(Db::sexses[account.sex]));
        }

        if (response_has_status)
        {
            writer.Key("status");
            writer.String(StringRef(Db::statuses[account.status]));
        }

        if (response_has_fname && account.fname != 0)
        {
            writer.Key("fname");
            writer.String(StringRef(fnames_dict[account.fname]));
        }

        if (response_has_sname && account.sname != 0)
        {
            writer.Key("sname");
            writer.String(StringRef(snames_dict[account.sname]));
        }

        if (response_has_city && account.city != 0)
        {
            writer.Key("city");
            writer.String(StringRef(cities_dict[account.city]));
        }

        if (response_has_country && account.country != 0)
        {
            writer.Key("country");
            writer.String(StringRef(countries_dict[account.country]));
        }

        if (response_has_birth)
        {
            writer.Key("birth");
            writer.Uint(account.birth);
        }

        if (response_has_phone)
        {
            writer.Key("phone");
            writer.String(StringRef(account.phone));
        }

        if (response_has_premium && account.premium_start != 0)
        {
            writer.Key("premium");
            writer.StartObject();
            writer.Key("start");
            writer.Uint(account.premium_start);
            writer.Key("finish");
            writer.Uint(account.premium_finish);
            writer.EndObject();
        }

        writer.EndObject();

        return found_accounts < limit;
    });

    // if (checked_accounts > limit)
    // {
    //     for (const auto &[key, value] : query)
    //     {
    //         printf("%s=%s ", string(key).c_str(), string(value).c_str());
    //     }
    //     printf("\nIndex: %lu\n", get_size(index));
    //     printf("Scanned: %d\n", checked_accounts);
    //     printf("Found: %d\n", found_accounts);
    //     printf("\n");
    // }

    writer.EndArray();
    writer.EndObject();

    return 200;
}
