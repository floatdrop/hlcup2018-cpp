#include "db.h"

template <class V>
void combo(const std::vector<V> &c, int k, std::function<void(std::vector<V> &)> f)
{
    int n = c.size();
    std::vector<V> res(n);
    int combo = (1 << k) - 1; // k bit sets
    while (combo < 1 << n)
    {
        int p = 0;
        for (int i = 0; i < n; ++i)
        {
            if ((combo >> i) & 1)
            {
                res[p++] = c[i];
            }
        }
        f(res);

        int x = combo & -combo;
        int y = combo + x;
        int z = (combo & ~y);
        combo = z / x;
        combo >>= 1;
        combo |= y;
    }
}

struct Timer
{
    std::chrono::system_clock::time_point start;
    std::function<void(std::chrono::nanoseconds)> cb;
    Timer(std::function<void(std::chrono::nanoseconds)> cb) : start(std::chrono::system_clock::now()), cb(cb) {}
    ~Timer()
    {
        cb(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now() - start));
    }
};

void Db::reindex()
{
    {
        Timer timer([&](std::chrono::nanoseconds elapsed) {
            printf("Clearing: %f\n", double(elapsed.count()) / 1000000.0);
        });
        for (auto &[domain, idx] : domain_index)
        {
            idx.clear();
        }

        for (auto &[domain, idx] : email_prefix_index)
        {
            idx.clear();
        }
        email_prefixes.clear();

        for (auto &idx : sex_index)
        {
            idx.clear();
        }

        for (auto &idx : status_index)
        {
            idx.clear();
        }

        for (auto &[t, idx] : country_sex_status_index)
        {
            idx.clear();
        }

        for (auto &[t, idx] : city_sex_status_index)
        {
            idx.clear();
        }

        for (auto &idx : status_neq_index)
        {
            idx.clear();
        }

        for (auto &idx : city_index)
        {
            idx.clear();
        }

        city_nnull_index.clear();

        for (auto &idx : country_index)
        {
            idx.clear();
        }

        country_nnull_index.clear();

        for (auto &[birth, idx] : birth_index)
        {
            idx.clear();
        }

        for (auto &idx : fname_index)
        {
            idx.clear();
        }
        fname_nnull_index.clear();

        for (auto &idx : sname_index)
        {
            idx.clear();
        }
        sname_nnull_index.clear();

        for (auto &[phone, idx] : phone_index)
        {
            idx.clear();
        }
        phone_nnull_index.clear();

        premium_null_index.clear();
        premium_nnull_index.clear();
        premium_now_index.clear();
        premium_sex_status_interest_index.clear();

        for (auto &idx : interested_index)
        {
            idx.clear();
        }
        for (auto &[t, idx] : interested_2_index)
        {
            idx.clear();
        }
        for (auto &[t, idx] : interested_5_index)
        {
            idx.clear();
        }
        interests_grouped_counters.clear();
        grouped_counters = GroupCounters();
    };

    {
        Timer timer([&](std::chrono::nanoseconds elapsed) {
            printf("Ids_index rebuilding: %f\n", double(elapsed.count()) / 1000000.0);
        });

        ids_index.clear();
        for (size_t i = accounts.size(); i > 0; --i)
        {
            if (account_exist(i))
            {
                ids_index.push_back(i);
            }
        }
        sort(ids_index.begin(), ids_index.end(), std::greater<uint32_t>());
    };

    {
        Timer timer([&](std::chrono::nanoseconds elapsed) {
            printf("Indexes rebuild: %f\n", double(elapsed.count()) / 1000000.0);
        });

        for (const auto &id : ids_index)
        {
            const auto &a = accounts[id];
            domain_index[domain_dict[parse_email_domain(a.email)]].push_back(id);
            auto prefix = a.email.substr(0, 2);
            if (email_prefix_index[prefix].size() == 0)
            {
                email_prefixes.push_back(prefix);
            }
            email_prefix_index[prefix].push_back(id);
            sex_index[a.sex].push_back(id);

            status_index[a.status].push_back(id);
            status_neq_index[(a.status % 3) + 1].push_back(id);
            status_neq_index[(a.status + 1) % 3 + 1].push_back(id);

            country_sex_status_index[{a.country, a.sex, a.status}].push_back(id);
            city_sex_status_index[{a.city, a.sex, a.status}].push_back(id);

            city_index[a.city].push_back(id);
            if (a.city != 0)
            {
                city_nnull_index.push_back(id);
            }
            country_index[a.country].push_back(id);
            if (a.country != 0)
            {
                country_nnull_index.push_back(id);
            }

            birth_index[year_from_ts(a.birth)].push_back(id);

            fname_index[a.fname].push_back(id);
            if (a.fname != 0)
            {
                fname_nnull_index.push_back(id);
            }

            sname_index[a.sname].push_back(id);
            if (a.sname != 0)
            {
                sname_nnull_index.push_back(id);
            }
            phone_index[a.pcode].push_back(id);
            if (a.pcode != 0)
            {
                phone_nnull_index.push_back(id);
            }

            if (a.premium_start == 0)
            {
                premium_null_index.push_back(id);
            }
            else
            {
                premium_nnull_index.push_back(id);
                if (a.premium_now)
                {
                    premium_now_index.push_back(id);
                }
            }
        }

        std::sort(email_prefixes.begin(), email_prefixes.end());
    };

    {
        Timer timer([&](std::chrono::nanoseconds elapsed) {
            printf("Interested rebuild: %f\n", double(elapsed.count()) / 1000000.0);
        });
        std::vector<uint8_t> interests;
        interests.reserve(8);
        for (const auto &id : ids_index)
        {
            const auto &a = accounts[id];
            interests.clear();
            for (size_t i = 0; i < 128; i++)
            {
                if (a.interests_bits.test(i))
                {
                    interests.push_back(i);
                    interested_index[i].push_back(id);
                    premium_sex_status_interest_index[{a.premium_now, a.sex, a.status, i}].push_back(id);
                }
            }

            if (interests.size() > 1)
            {
                combo<uint8_t>(interests, 2, [&](std::vector<uint8_t> &combo) {
                    interested_2_index[{combo[0], combo[1]}].push_back(id);
                });
            }

            if (interests.size() > 4)
            {
                combo<uint8_t>(interests, 5, [&](std::vector<uint8_t> &combo) {
                    interested_5_index[{combo[0], combo[1], combo[2], combo[3], combo[4]}].push_back(id);
                });
            }
        }

    };

    {
        Timer timer([&](std::chrono::nanoseconds elapsed) {
            printf("Liked rebuild: %f\n", double(elapsed.count()) / 1000000.0);
        });

        // for (auto &index : liked_index)
        // {
        //     std::sort(index.begin(), index.end(), std::greater<uint32_t>());
        //     index.erase(unique(index.begin(), index.end()), index.end());
        // }

        for (auto &indexes : liked_sex_index)
        {
            for (auto &index : indexes)
            {
                std::sort(index.begin(), index.end(), std::greater<uint32_t>());
                index.erase(unique(index.begin(), index.end()), index.end());
            }
        }
    };

    {
        Timer timer([&](std::chrono::nanoseconds elapsed) {
            printf("GCounters rebuild: %f\n", double(elapsed.count()) / 1000000.0);
        });
        for (const auto &id : ids_index)
        {
            const auto &a = accounts[id];
            const auto birth_year = year_from_ts(a.birth) - 1970;
            const auto joined_year = year_from_ts(a.joined) - 2000;

            grouped_counters.birth[birth_year]++;
            grouped_counters.joined[joined_year]++;

            grouped_counters.sex[a.sex]++;
            grouped_counters.sex_birth[a.sex][birth_year]++;
            grouped_counters.sex_joined[a.sex][joined_year]++;

            grouped_counters.status[a.status]++;
            grouped_counters.status_birth[a.status][birth_year]++;
            grouped_counters.status_joined[a.status][joined_year]++;

            grouped_counters.city[a.city]++;
            grouped_counters.city_birth[a.city][birth_year]++;
            grouped_counters.city_joined[a.city][joined_year]++;
            grouped_counters.city_sex_status[a.city][a.sex][a.status]++;
            grouped_counters.city_sex_status_birth[a.city][a.sex][a.status][birth_year]++;
            grouped_counters.city_sex_status_joined[a.city][a.sex][a.status][joined_year]++;

            grouped_counters.country[a.country]++;
            grouped_counters.country_birth[a.country][birth_year]++;
            grouped_counters.country_joined[a.country][joined_year]++;
            grouped_counters.country_sex_status[a.country][a.sex][a.status]++;
            grouped_counters.country_sex_status_birth[a.country][a.sex][a.status][birth_year]++;
            grouped_counters.country_sex_status_joined[a.country][a.sex][a.status][joined_year]++;
        }
    };

    {
        Timer timer([&](std::chrono::nanoseconds elapsed) {
            printf("ICounters rebuild: %f\n", double(elapsed.count()) / 1000000.0);
        });
        for (const auto &id : ids_index)
        {
            const auto &a = accounts[id];
            const auto birth_year = year_from_ts(a.birth) - 1970;
            const auto joined_year = year_from_ts(a.joined) - 2000;

            for (size_t i = 0; i < 128; i++)
            {
                if (a.interests_bits.test(i))
                {
                    auto &counters = interests_grouped_counters[i];

                    counters.total_count++;

                    counters.birth[birth_year]++;
                    counters.joined[joined_year]++;

                    counters.city[a.city]++;
                    counters.city_birth[a.city][birth_year]++;
                    counters.city_joined[a.city][joined_year]++;

                    counters.city_sex_status[a.city][a.sex][a.status]++;
                    counters.city_sex_status_birth[a.city][a.sex][a.status][birth_year]++;
                    counters.city_sex_status_joined[a.city][a.sex][a.status][joined_year]++;

                    counters.country[a.country]++;
                    counters.country_birth[a.country][birth_year]++;
                    counters.country_joined[a.country][joined_year]++;

                    counters.country_sex_status[a.country][a.sex][a.status]++;
                    counters.country_sex_status_birth[a.country][a.sex][a.status][birth_year]++;
                    counters.country_sex_status_joined[a.country][a.sex][a.status][joined_year]++;
                }
            }
        }
    };

    // printf("\cities:\n");
    // for (size_t i = 1; i < city_index.size(); ++i)
    // {
    //     printf("%s: %d\n", std::string(cities_dict[i]).c_str(), city_index[i].size());
    // }
    // printf("\ncountries:\n");
    // for (size_t i = 1; i < country_index.size(); ++i)
    // {
    //     printf("%s: %d\n", std::string(countries_dict[i]).c_str(), country_index[i].size());
    // }
    // printf("\nsnames:\n");
    // for (size_t i = 1; i < snames_dict.size(); ++i)
    // {
    //     printf("%s: %d\n", std::string(snames_dict[i]).c_str(), sname_index[i].size());
    // }

    // for (size_t i = 1; i < interested_index.size(); ++i)
    // {
    //     printf("%s: %d\n", std::string(interests_dict[i]).c_str(), interested_index[i].size());
    // }

    // for (auto prefix : email_prefixes)
    // {
    //     printf("%s: %d\n", std::string(prefix).c_str(), email_prefix_index[prefix].size());
    // }
};
