#include <sys/resource.h>

#include <iostream>
#include <fstream>
#include "server_http.hpp"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/bundled/ostream.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/writer.h"
#include "absl/synchronization/mutex.h"

#include "extractor/extractor.h"
#include "db/db.h"

// #define LOG_TIMERS 1

static uint64_t WARNING_NS_THRESHOLD = 1000000;

using namespace std;
using namespace rapidjson;

using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

static thread_local StringBuffer buffer(0, 4096);

struct Timer
{
  std::chrono::system_clock::time_point start;
  std::function<void(std::chrono::nanoseconds)> cb;
  Timer(std::function<void(std::chrono::nanoseconds)> cb) : start(std::chrono::system_clock::now()), cb(cb) {}
  ~Timer()
  {
    cb(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now() - start));
  }
};

absl::string_view
as_sv(std::ssub_match m)
{
  if (!m.matched)
    return {};
  return {&*m.first, static_cast<size_t>(m.second - m.first)};
}

int main()
{
  auto log = spdlog::stdout_color_mt("server");

  struct rlimit l;
  l.rlim_cur = 4096;
  l.rlim_max = 4096;
  if (setrlimit(RLIMIT_NOFILE, &l) < 0)
  {
    log->error("setrlimit failed: {} ({})", errno, strerror(errno));
  }
  else
  {
    log->info("setrlimit set to: {}", l.rlim_max);
  }

  // spdlog::set_level(spdlog::level::level_enum::err);

  int raiting_run = 0;
  string last_post_query_id = "9999";

  {
    ifstream options_file("/tmp/data/options.txt", ifstream::in);
    if (!options_file.is_open())
    {
      log->error("Failed to open /tmp/data/options.txt!");
      return 1;
    }
    options_file >> Db::current_time;
    options_file >> raiting_run;
  }

  if (raiting_run)
  {
    log->warn("Expecting raiting run.");
    last_post_query_id = "89999";
  }

  Db db;

  auto start_extracting = std::chrono::system_clock::now();
  size_t accounts = 0;

  if (extract("/tmp/data/data.zip", [&](Document &d) {
        auto &a = d["accounts"];
        using Iterator = rapidjson::Value::ValueIterator;
        using ReverseIterator = std::reverse_iterator<Iterator>;

        ReverseIterator rbegin(a.End());
        ReverseIterator rend(a.Begin());

        for (auto v = rbegin; v != rend; ++v)
        {
          accounts++;
          Document account;
          account.CopyFrom(*v, d.GetAllocator());
          db.insert(account);
        }
      }) != 0)
  {
    return 1;
  };

  auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start_extracting);
  log->info("Extracted {} accounts in {}s", accounts, elapsed.count() / 1000.0);

  {
    log->info("Starting reindex");
    Timer timer([&](std::chrono::nanoseconds elapsed) {
      log->info("Reindex took {}s", double(elapsed.count()) / 1000000000.0);
    });

    db.reindex();
  }

  db.index_stats();

  absl::Mutex mutex;

  HttpServer server;
  server.config.reuse_address = true;
  server.config.port = 8080;

  if (const char *env_p = getenv("PORT"))
  {
    uint32_t port;
    if (!absl::SimpleAtoi(env_p, &port))
    {
      log->error("Invalid PORT environment variable");
      return 1;
    }
    server.config.port = port;
  }

  server.config.thread_pool_size = 4;
  server.config.timeout_request = 5;

  SimpleWeb::CaseInsensitiveMultimap server_header{
      {"Server", "sws"},
      {"Content-Type", "application/json; charset=utf-8"}};

  server.resource["/accounts/filter/"]["GET"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
#ifdef LOG_TIMERS
    Timer timer([&](std::chrono::nanoseconds elapsed) {
      if (elapsed.count() > WARNING_NS_THRESHOLD)
      {
        log->warn("{} {}?{} - {}", request->method, request->path, request->query_string, double(elapsed.count()) / 1000000.0);
      }
    });
#endif

    auto query_fields = request->parse_query_string();

    // absl::ReaderMutexLock lock(&mutex);

    buffer.Clear();
    Writer<StringBuffer> writer(buffer);
    if (db.filter(query_fields, writer) != 400)
    {
      response->write(std::string_view(buffer.GetString(), buffer.GetSize()), server_header);
    }
    else
    {
      // log->error("Error processing request: {0}", e.what());
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "", server_header);
    }
  };

  server.resource["/accounts/group/"]["GET"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
#ifdef LOG_TIMERS
    Timer timer([&](std::chrono::nanoseconds elapsed) {
      if (elapsed.count() > WARNING_NS_THRESHOLD)
      {
        log->warn("{} {}?{} - {}", request->method, request->path, request->query_string, double(elapsed.count()) / 1000000.0);
      }
    });
#endif

    auto query_fields = request->parse_query_string();

    // absl::ReaderMutexLock lock(&mutex);

    buffer.Clear();
    Writer<StringBuffer> writer(buffer);
    if (db.group(query_fields, writer) != 400)
    {
      response->write(std::string_view(buffer.GetString(), buffer.GetSize()), server_header);
    }
    else
    {
      // log->error("{} {}?{}: {}", request->method, request->path, request->query_string, e.what());
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "", server_header);
    }
  };

  server.resource["/accounts/([0-9]+)/recommend/"]["GET"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
#ifdef LOG_TIMERS
    Timer timer([&](std::chrono::nanoseconds elapsed) {
      if (elapsed.count() > WARNING_NS_THRESHOLD)
      {
        log->warn("{} {}?{} - {}", request->method, request->path, request->query_string, double(elapsed.count()) / 1000000.0);
      }
    });
#endif

    // log->info("{} {}?{}", request->method, request->path, request->query_string);

    auto query_fields = request->parse_query_string();

    uint32_t id;
    if (!absl::SimpleAtoi(as_sv(request->path_match[1]), &id))
    {
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "", server_header);
      return;
    }

    // absl::ReaderMutexLock lock(&mutex);

    if (!db.account_exist(id))
    {
      response->write(SimpleWeb::StatusCode::client_error_not_found, "{}", server_header);
      return;
    }

    buffer.Clear();
    Writer<StringBuffer> writer(buffer);
    if (db.recommend(id, query_fields, writer) == 200)
    {
      response->write(std::string_view(buffer.GetString(), buffer.GetSize()), server_header);
    }
    else
    {
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "", server_header);
    }
  };

  server.resource["/accounts/([0-9]+)/suggest/"]["GET"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
#ifdef LOG_TIMERS
    Timer timer([&](std::chrono::nanoseconds elapsed) {
      if (elapsed.count() > WARNING_NS_THRESHOLD)
      {
        log->warn("{} {}?{} - {}", request->method, request->path, request->query_string, double(elapsed.count()) / 1000000.0);
      }
    });
#endif

    auto query_fields = request->parse_query_string();

    uint32_t id;
    if (!absl::SimpleAtoi(as_sv(request->path_match[1]), &id))
    {
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "", server_header);
      return;
    }

    // absl::ReaderMutexLock lock(&mutex);

    if (!db.account_exist(id))
    {
      response->write(SimpleWeb::StatusCode::client_error_not_found, "{}", server_header);
      return;
    }

    buffer.Clear();
    Writer<StringBuffer> writer(buffer);
    if (db.suggest(id, query_fields, writer) == 200)
    {
      response->write(std::string_view(buffer.GetString(), buffer.GetSize()), server_header);
    }
    else
    {
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "", server_header);
    }
  };

  server.resource["/accounts/new/"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
  // log->info("{} {}?{}", request->method, request->path, request->query_string);
#ifdef LOG_TIMERS
    Timer timer([&](std::chrono::nanoseconds elapsed) {
      if (elapsed.count() > WARNING_NS_THRESHOLD)
      {
        log->warn("{} {}?{} - {}", request->method, request->path, request->query_string, double(elapsed.count()) / 1000000.0);
      }
    });
#endif

    absl::MutexLock lock(&mutex);

    Document d;
    IStreamWrapper isw(request->content);
    d.ParseStream(isw);

    if (db.insert(d) != 400)
    {
      response->write(SimpleWeb::StatusCode::success_created, "{}", server_header);
    }
    else
    {
      // log->error("{} {}?{}: {}", request->method, request->path, request->query_string, e.what());
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "", server_header);
    }

    if (request->parse_query_string().find("query_id")->second == last_post_query_id)
    {
      std::thread([&]() {
        log->info("Starting reindex");
        Timer timer([&](std::chrono::nanoseconds elapsed) {
          log->info("Reindex took {}s", double(elapsed.count()) / 1000000000.0);
        });
        absl::MutexLock lock(&mutex);

        db.reindex();
      })
          .detach();
    }
  };

  server.resource["/accounts/likes/"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
  // log->info("{} {}?{}", request->method, request->path, request->query_string);
#ifdef LOG_TIMERS
    Timer timer([&](std::chrono::nanoseconds elapsed) {
      if (elapsed.count() > WARNING_NS_THRESHOLD)
      {
        log->warn("{} {}?{} - {}", request->method, request->path, request->query_string, double(elapsed.count()) / 1000000.0);
      }
    });
#endif

    absl::MutexLock lock(&mutex);

    Document d;
    IStreamWrapper isw(request->content);
    d.ParseStream(isw);
    if (db.likes(d) != 400)
    {
      response->write(SimpleWeb::StatusCode::success_accepted, "{}", server_header);
    }
    else
    {
      // log->error("{} {}?{}: {}", request->method, request->path, request->query_string, e.what());
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "", server_header);
    }

    if (request->parse_query_string().find("query_id")->second == last_post_query_id)
    {
      std::thread([&]() {
        log->info("Starting reindex");
        Timer timer([&](std::chrono::nanoseconds elapsed) {
          log->info("Reindex took {}s", double(elapsed.count()) / 1000000000.0);
        });
        absl::MutexLock lock(&mutex);

        db.reindex();
      })
          .detach();
    }
  };

  server.resource["/accounts/([0-9]+)/"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
  // log->info("{} {}?{}", request->method, request->path, request->query_string);
#ifdef LOG_TIMERS
    Timer timer([&](std::chrono::nanoseconds elapsed) {
      if (elapsed.count() > WARNING_NS_THRESHOLD)
      {
        log->warn("{} {}?{} - {}", request->method, request->path, request->query_string, double(elapsed.count()) / 1000000.0);
      }
    });
#endif

    absl::MutexLock lock(&mutex);

    uint32_t id;
    if (!absl::SimpleAtoi(as_sv(request->path_match[1]), &id))
    {
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "", server_header);
      return;
    }

    if (!db.account_exist(id))
    {
      response->write(SimpleWeb::StatusCode::client_error_not_found, "{}", server_header);
      return;
    }

    Document d;
    IStreamWrapper isw(request->content);
    d.ParseStream(isw);
    if (db.update(id, d) != 400)
    {
      response->write(SimpleWeb::StatusCode::success_accepted, "{}", server_header);
    }
    else
    {
      // log->error("{} {}?{}: {}", request->method, request->path, request->query_string, e.what());
      response->write(SimpleWeb::StatusCode::client_error_bad_request, "", server_header);
    }

    if (request->parse_query_string().find("query_id")->second == last_post_query_id)
    {
      std::thread([&]() {
        log->info("Starting reindex");
        Timer timer([&](std::chrono::nanoseconds elapsed) {
          log->info("Reindex took {}s", double(elapsed.count()) / 1000000000.0);
        });
        absl::MutexLock lock(&mutex);

        db.reindex();
      })
          .detach();
    }
  };

  server.default_resource["GET"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
    response->write(SimpleWeb::StatusCode::client_error_not_found, "", server_header);
  };

  server.default_resource["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
    response->write(SimpleWeb::StatusCode::client_error_not_found, "", server_header);
  };

  server.on_error = [&](shared_ptr<HttpServer::Request> request, const SimpleWeb::error_code &ec) {
    // log->error("Error processing request {0}: {1}", request->path, ec);
    // Handle errors here
    // Note that connection timeouts will also call this handle with ec set to SimpleWeb::errc::operation_canceled
  };

  log->info("Started server on localhost:{0}", server.config.port);

  server.start();
}
