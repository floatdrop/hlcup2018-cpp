#include "gtest/gtest.h"
#include "idmap/idmap.h"

TEST(IdMap, GettingKeyValues)
{
    IdMap idmap;
    EXPECT_EQ(idmap["Bazel"], 1);
    EXPECT_EQ(idmap["Bazel"], 1);
    EXPECT_EQ(idmap["Gazel"], 2);
    EXPECT_EQ(idmap["Gazel"], 2);
    EXPECT_EQ(idmap[0], "");
    EXPECT_EQ(idmap[1], "Bazel");
    EXPECT_EQ(idmap[2], "Gazel");
}
