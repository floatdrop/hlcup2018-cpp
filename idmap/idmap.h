#pragma once

#include "absl/container/flat_hash_map.h"

class IdMap
{
  public:
    absl::flat_hash_map<std::string, size_t> id;
    std::vector<std::string> values;

    IdMap()
    {
        values.push_back("");
        values.reserve(2048);
    }

    size_t size() const
    {
        return values.size();
    }

    bool contains(size_t idx) const
    {
        return 0 < idx && idx < values.size();
    }

    bool contains(const std::string_view value) const
    {
        return id.find(value) != id.end();
    }

    std::string_view operator[](size_t idx)
    {
        return values[idx];
    }

    std::string_view operator[](size_t idx) const
    {
        return values[idx];
    }

    size_t &operator[](const std::string_view value)
    {
        if (id.find(value) == id.end())
        {
            size_t key = values.size();
            values.push_back(std::string(value));
            id[value] = key;
        }
        return id[value];
    }

    size_t operator[](const std::string_view value) const
    {
        return std::distance(id.begin(), id.find(value));
    }
};
