#include <benchmark/benchmark.h>
#include "db/db.h"
#include <vector>
#include <unistd.h>
#include <random>
#include <algorithm>
#include <queue>
#include "db/conduit.hpp"
#include <iterator>

using namespace conduit;
using namespace conduit::operators;

static void BM_HEAP_UNION(benchmark::State &state)
{
    size_t SETS_NUMBER = state.range(0);
    std::vector<id_set> sets(SETS_NUMBER);
    std::vector<id_set *> indexes;
    for (size_t n = 0; n < SETS_NUMBER; ++n)
    {
        for (auto i = 150000 / SETS_NUMBER; i > 0; --i)
        {
            sets[n].insert(i);
        }
        indexes.push_back(&sets[n]);
    }

    for (auto _ : state)
    {
        heap_union(indexes);
    }
}
BENCHMARK(BM_HEAP_UNION)->RangeMultiplier(2)->Range(1, 256);

static void BM_CONDUIT_HEAP(benchmark::State &state)
{
    size_t SETS_NUMBER = state.range(0);
    std::vector<id_set> sets(SETS_NUMBER);
    for (size_t n = 0; n < SETS_NUMBER; ++n)
    {
        for (auto i = 150000 / SETS_NUMBER; i > 0; --i)
        {
            sets[n].insert(i);
        }
    }

    auto setsUnion = [](std::vector<id_set> &sets) -> seq<uint32_t> {
        std::priority_queue<Range> heads;

        for (auto &set : sets)
        {
            heads.emplace(set.begin(), set.end());
        }

        while (!heads.empty())
        {
            Range top = heads.top();
            co_yield top.value;
            heads.pop();

            top.begin++;
            if (top.begin != top.end)
            {
                heads.emplace(top.begin, top.end);
            }
        }
    };

    for (auto _ : state)
    {
        benchmark::DoNotOptimize(setsUnion(sets) >> toVector(150000));
    }
}
BENCHMARK(BM_CONDUIT_HEAP)->RangeMultiplier(2)->Range(1, 256);

// static void BM_HEAP_INTERSECTION_TAKE_50(benchmark::State &state)
// {
//     size_t range0 = state.range(0);

//     std::vector<std::vector<uint32_t>> sets;
//     for (auto size = 0; size < range0; size++)
//     {
//         std::vector<uint32_t> set;
//         auto l = size == 0 ? 10 : 40000;
//         for (int i = 0; i < l; i++)
//         {
//             set.push_back(rand() % 40000);
//         }
//         std::sort(set.begin(), set.end(), [](const uint32_t a, const uint32_t b) {
//             return a > b;
//         });
//         sets.push_back(set);
//     }

//     for (auto _ : state)
//     {
//         std::vector<uint32_t> y;
//         y.reserve(50);

//         KSetIntersection u(sets);
//         for (auto i = u.begin(); i != u.end(); ++i)
//         {
//             if (y.size() > 50)
//             {
//                 break;
//             }
//             y.push_back(*i);
//         }
//     }
// }
// BENCHMARK(BM_HEAP_INTERSECTION_TAKE_50)->RangeMultiplier(2)->Range(2, 64);

BENCHMARK_MAIN();
