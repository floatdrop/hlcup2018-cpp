#include "gtest/gtest.h"
#include "db/db.h"

TEST(heap_union, basic)
{
    std::vector<id_set *> v = {
        new id_set({3, 2, 1}),
        new id_set({5, 4, 0}),
        new id_set({8, 7, 6})};

    auto y = heap_union(v);

    ASSERT_EQ(y.size(), 9);
    ASSERT_EQ((y.find(8) != y.end()), true);
    ASSERT_EQ((y.find(7) != y.end()), true);
    ASSERT_EQ((y.find(6) != y.end()), true);
    ASSERT_EQ((y.find(5) != y.end()), true);
    ASSERT_EQ((y.find(4) != y.end()), true);
    ASSERT_EQ((y.find(3) != y.end()), true);
    ASSERT_EQ((y.find(2) != y.end()), true);
    ASSERT_EQ((y.find(1) != y.end()), true);
    ASSERT_EQ((y.find(0) != y.end()), true);
}

TEST(heap_union, one_empty)
{
    std::vector<id_set *> v = {
        new id_set({}),
        new id_set({5, 4, 0})};

    auto y = heap_union(v);

    ASSERT_EQ(y.size(), 3);
    ASSERT_EQ((y.find(5) != y.end()), true);
    ASSERT_EQ((y.find(4) != y.end()), true);
    ASSERT_EQ((y.find(0) != y.end()), true);
}


TEST(heap_union, unique)
{
    std::vector<id_set *> v = {
        new id_set({5, 3, 1}),
        new id_set({5, 4, 1})};

    auto y = heap_union(v);

    ASSERT_EQ(y.size(), 4);
    ASSERT_EQ((y.find(5) != y.end()), true);
    ASSERT_EQ((y.find(4) != y.end()), true);
    ASSERT_EQ((y.find(3) != y.end()), true);
    ASSERT_EQ((y.find(1) != y.end()), true);
}

TEST(heap_intersection, basic)
{
    std::vector<id_set *> v = {
        new id_set({8, 3}),
        new id_set({8, 5, 3}),
        new id_set({10, 8, 3, 1})};

    id_set result;

    heap_intersection(v, [&](uint32_t id) { result.insert(id); return true; });

    ASSERT_EQ(result.size(), 2);
    ASSERT_EQ(result.find(8) != result.end(), true);
    ASSERT_EQ(result.find(3) != result.end(), true);
}

TEST(heap_intersection, hot_start)
{
    std::vector<id_set *> v = {
        new id_set({8, 3}),
        new id_set({8, 5, 3}),
        new id_set({8, 3, 1})};

    id_set result;

    heap_intersection(v, [&](uint32_t id) { result.insert(id); return true; });

    ASSERT_EQ(result.size(), 2);
    ASSERT_EQ(result.find(8) != result.end(), true);
    ASSERT_EQ(result.find(3) != result.end(), true);
}

TEST(heap_intersection, hot_end)
{
    std::vector<id_set *> v = {
        new id_set({8, 3}),
        new id_set({8, 5, 3}),
        new id_set({8, 3})};

    id_set result;

    heap_intersection(v, [&](uint32_t id) { result.insert(id); return true; });

    ASSERT_EQ(result.size(), 2);
    ASSERT_EQ(result.find(8) != result.end(), true);
    ASSERT_EQ(result.find(3) != result.end(), true);
}

TEST(heap_intersection, hot_start_middle_end)
{
    std::vector<id_set *> v = {
        new id_set({8, 5, 3}),
        new id_set({8, 5, 3}),
        new id_set({8, 5, 3})};

    id_set result;

    heap_intersection(v, [&](uint32_t id) { result.insert(id); return true; });

    ASSERT_EQ(result.size(), 3);
    ASSERT_EQ(result.find(8) != result.end(), true);
    ASSERT_EQ(result.find(5) != result.end(), true);
    ASSERT_EQ(result.find(3) != result.end(), true);
}

TEST(heap_intersection, multiskip)
{
    std::vector<id_set *> v = {
        new id_set({10, 9, 8, 3}),
        new id_set({8, 1})};

    id_set result;

    heap_intersection(v, [&](uint32_t id) { result.insert(id); return true; });

    ASSERT_EQ(result.size(), 1);
    ASSERT_EQ(result.find(8) != result.end(), true);
}

TEST(heap_intersection, single)
{
    std::vector<id_set *> v = {
        new id_set({8})};

    id_set result;

    heap_intersection(v, [&](uint32_t id) { result.insert(id); return true; });

    ASSERT_EQ(result.size(), 1);
    ASSERT_EQ(result.find(8) != result.end(), true);
}

TEST(heap_intersection, empty)
{
    std::vector<id_set *> v = {};

    id_set result;

    heap_intersection(v, [&](uint32_t id) { result.insert(id); return true; });

    ASSERT_EQ(result.size(), 0);
}

TEST(heap_intersection, emptyempty)
{
    std::vector<id_set *> v = {new id_set()};

    id_set result;

    heap_intersection(v, [&](uint32_t id) { result.insert(id); return true; });

    ASSERT_EQ(result.size(), 0);
}

TEST(heap_intersection, single_long)
{
    std::vector<uint32_t> ints;
    ints.reserve(1500000);
    for (size_t i = 1500000; i > 0; i--)
    {
        ints.push_back(i);
    }

    id_set long_set;
    long_set.adopt_sequence(boost::container::ordered_unique_range_t(), std::move(ints));

    std::vector<id_set *> v = {&long_set};

    auto result = heap_intersection(v);

    ASSERT_EQ(result.size(), long_set.size());
}
