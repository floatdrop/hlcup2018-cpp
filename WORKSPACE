workspace(name = "hlcup2018")

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")

git_repository(
    name = "com_google_absl",
    # 2018-12-27
    commit = "7ffbe09f3d85504bd018783bbe1e2c12992fe47c",
    remote = "https://github.com/abseil/abseil-cpp.git",
)

http_archive(
    name = "com_github_google_googletest",
    sha256 = "9bf1fe5182a604b4135edc1a425ae356c9ad15e9b23f9f12a02e80184c3a249c",
    strip_prefix = "googletest-release-1.8.1",
    urls = ["https://github.com/google/googletest/archive/release-1.8.1.tar.gz"],
)

http_archive(
    name = "com_github_google_benchmark",
    sha256 = "f8e525db3c42efc9c7f3bc5176a8fa893a9a9920bbd08cef30fb56a51854d60d",
    strip_prefix = "benchmark-1.4.1",
    urls = ["https://github.com/google/benchmark/archive/v1.4.1.tar.gz"],
)

http_archive(
    name = "com_github_rapidjson",
    build_file = "@hlcup2018//bazel/external:rapidjson.BUILD",
    sha256 = "bf7ced29704a1e696fbccf2a2b4ea068e7774fa37f6d7dd4039d0787f8bed98e",
    strip_prefix = "rapidjson-1.1.0",
    urls = ["https://github.com/Tencent/rapidjson/archive/v1.1.0.tar.gz"],
)

http_archive(
    name = "com_gitlab_eidheim_simple-web-server",
    build_file = "@hlcup2018//bazel/external:simple-web-server.BUILD",
    strip_prefix = "Simple-Web-Server-master",
    urls = ["https://gitlab.com/floatdrop/Simple-Web-Server/-/archive/master/Simple-Web-Server-master.tar.gz"],
)

# http_archive(
#     name = "com_gitlab_eidheim_simple-web-server",
#     build_file = "@hlcup2018//bazel/external:simple-web-server.BUILD",
#     sha256 = "b6230fb1f267cf7f45b6b8eb6efb3ed6a4308fd48003dc8397eba1d7cf048467",
#     strip_prefix = "Simple-Web-Server-v3.0.0-rc3",
#     urls = ["https://gitlab.com/eidheim/Simple-Web-Server/-/archive/v3.0.0-rc3/Simple-Web-Server-v3.0.0-rc3.tar.gz"],
# )

http_archive(
    name = "com_gitlab_richgel999_miniz",
    build_file = "@hlcup2018//bazel/external:miniz.BUILD",
    sha256 = "de0525648c0aa69e9f98e25d8c91bcc192c1704d92c47d8457062a184ac2c492",
    strip_prefix = "miniz-2.0.8",
    urls = ["https://github.com/richgel999/miniz/archive/2.0.8.tar.gz"],
)

http_archive(
    name = "com_github_gabime_spdlog",
    build_file = "@hlcup2018//bazel/external:spdlog.BUILD",
    sha256 = "867a4b7cedf9805e6f76d3ca41889679054f7e5a3b67722fe6d0eae41852a767",
    strip_prefix = "spdlog-1.2.1",
    urls = ["https://github.com/gabime/spdlog/archive/v1.2.1.tar.gz"],
)

http_archive(
    name = "com_github_Cyan4973_xxHash",
    build_file = "@hlcup2018//bazel/external:xxhash.BUILD",
    sha256 = "19030315f4fc1b4b2cdb9d7a317069a109f90e39d1fe4c9159b7aaa39030eb95",
    strip_prefix = "xxHash-0.6.5",
    urls = ["https://github.com/Cyan4973/xxHash/archive/v0.6.5.tar.gz"],
)

git_repository(
    name = "com_github_Tessil_hat-trie",
    commit = "38f00995a89b0e38bf7d8977b30b4a990e81fefc",
    remote = "https://github.com/floatdrop/hat-trie",
)

http_archive(
    name = "com_github_fmtlib_fmt",
    build_file = "@hlcup2018//bazel/external:fmtlib.BUILD",
    sha256 = "4c0741e10183f75d7d6f730b8708a99b329b2f942dad5a9da3385ab92bb4a15c",
    strip_prefix = "fmt-5.3.0",
    urls = ["https://github.com/fmtlib/fmt/releases/download/5.3.0/fmt-5.3.0.zip"],
)

http_archive(
    name = "com_github_RoaringBitmap_CRoaring",
    build_file = "@hlcup2018//bazel/external:roaring.BUILD",
    sha256 = "02f80850e856001dbcd2571d140bab3d2bfb1e5bcc1e3b271d4c4e964dd65518",
    strip_prefix = "CRoaring-0.2.59",
    urls = ["https://github.com/RoaringBitmap/CRoaring/archive/v0.2.59.tar.gz"],
)

http_archive(
    name = "com_github_LoopPerfect_conduit",
    build_file = "@hlcup2018//bazel/external:conduit.BUILD",
    sha256 = "d68b10ab8859b22509d6840cc98bed0bc6d9307132475564be056066ccb30a8c",
    strip_prefix = "conduit-0.1.1",
    urls = ["https://github.com/LoopPerfect/conduit/archive/v0.1.1.tar.gz"],
)

git_repository(
    name = "com_github_nelhage_rules_boost",
    commit = "3e29854b65896f3c4577e68c08425e64b3d7a852",
    remote = "https://github.com/floatdrop/rules_boost",
)

load("@com_github_nelhage_rules_boost//:boost/boost.bzl", "boost_deps")

boost_deps()
