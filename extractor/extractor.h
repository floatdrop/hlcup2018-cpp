#pragma once

#if defined(__GNUC__)
// Ensure we get the 64-bit variants of the CRT's file I/O calls
#ifndef _FILE_OFFSET_BITS
#define _FILE_OFFSET_BITS 64
#endif
#ifndef _LARGEFILE64_SOURCE
#define _LARGEFILE64_SOURCE 1
#endif
#endif

#include <stdio.h>
#include <string>
#include <vector>
#include <functional>
#include "miniz_zip.h"
#include "rapidjson/document.h"
#include "rapidjson/error/en.h"
#include "fmt/format.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/bundled/ostream.h"
#include "spdlog/sinks/stdout_color_sinks.h"

using namespace rapidjson;

int extract(const std::string &file, std::function<void(Document &)> insert)
{
  auto log = spdlog::stdout_color_mt("extract");
  mz_zip_archive zip_archive;
  memset(&zip_archive, 0, sizeof(zip_archive));

  mz_bool status = mz_zip_reader_init_file(&zip_archive, file.c_str(), 0);
  if (!status)
  {
    log->error("mz_zip_reader_init_file() failed: {}", mz_zip_get_error_string(mz_zip_get_last_error(&zip_archive)));
    return EXIT_FAILURE;
  }

  auto N = (size_t)mz_zip_reader_get_num_files(&zip_archive);
  for (size_t i = N; i > 0; --i)
  {
    size_t uncomp_size = 0;
    auto archive_filename = fmt::format("accounts_{}.json", i);

    void *p = mz_zip_reader_extract_file_to_heap(&zip_archive, archive_filename.c_str(), &uncomp_size, 0);
    if (!p)
    {
      log->error("mz_zip_reader_extract_file_to_heap() failed: {}", mz_zip_get_error_string(mz_zip_get_last_error(&zip_archive)));
      mz_zip_reader_end(&zip_archive);
      return EXIT_FAILURE;
    }

    Document d;
    d.Parse((const char *)p, uncomp_size);

    if (d.HasParseError())
    {
      log->error("Error parsing file {} (offset {}): {}", archive_filename, (unsigned)d.GetErrorOffset(),
                 GetParseError_En(d.GetParseError()));
      return EXIT_FAILURE;
    }

    insert(d);

    mz_free(p);
  }

  mz_zip_reader_end(&zip_archive);

  return EXIT_SUCCESS;
}
