#include "gtest/gtest.h"
#include "extractor.h"

TEST(Extractor, Extract)
{
    auto accounts = 0;
    auto cb = [&](const Value &account) {
        accounts++;
    };
    extract("/tmp/data/data.zip", cb);
    EXPECT_EQ(accounts, 30000);
}
