FROM ubuntu:18.10 as builder

RUN apt-get update && apt-get install -y pkg-config zip g++ zlib1g-dev unzip python wget git clang-7

RUN update-alternatives --install /usr/bin/cc cc /usr/bin/clang-7 100
RUN update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++-7 100

ENV CC /usr/bin/clang-7
ENV CXX /usr/bin/clang++-7

RUN wget https://github.com/bazelbuild/bazel/releases/download/0.21.0/bazel-0.21.0-installer-linux-x86_64.sh
RUN chmod +x bazel-0.21.0-installer-linux-x86_64.sh
RUN ./bazel-0.21.0-installer-linux-x86_64.sh

WORKDIR /home

# copy your source tree
COPY . ./

RUN bazel build //server:server -c opt --copt=-march=nehalem

FROM ubuntu:18.10

COPY --from=builder \
    /home/bazel-bin/server/server \
    /usr/local/bin/

EXPOSE 80

STOPSIGNAL SIGTERM

ENV LOG_LEVEL "error"
ENV PORT 80

CMD ["/usr/local/bin/server"]
# CMD ["sysctl -a | grep net.ipv4.tcp"]
